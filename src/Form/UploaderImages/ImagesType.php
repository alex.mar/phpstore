<?php

namespace App\Form\UploaderImages;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ImagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', TextType::class, [
                'label' => 'Директория',
                'attr' => [
                    'placeholder' => 'images/'
                ]
            ])
            ->add('images', FileType::class, [
                'multiple' => true,
                'label' => 'Картинки: ',
                'attr' => [
                    'accept' => 'images/*',
                    'class' => 'test'
                ]
            ]);
    }
}