<?php

namespace App\Service\BreadCrumbs;

use App\Entity\UrlType;
use App\Service\BreadCrumbs\Formatters\BreadCrumbsItemFormatterInterface;
use App\Service\BreadCrumbs\Formatters\CategoryItemFormatter;
use App\Service\BreadCrumbs\Formatters\DepartmentItemFormatter;
use App\Service\BreadCrumbs\Formatters\HomeItemFormatter;
use App\Service\BreadCrumbs\Formatters\ProductItemFormatter;
use App\UrlData\UrlData;

class BreadCrumbsService
{
    /** @var BreadCrumbsItemFormatterInterface[] */
    private $formatters;

    public function __construct(
        HomeItemFormatter $homeItemFormatter,
        DepartmentItemFormatter $departmentItemFormatter,
        CategoryItemFormatter $categoryItemFormatter,
        ProductItemFormatter $productItemFormatter
    ) {
        $this->formatters = [
            $homeItemFormatter::getKey() => $homeItemFormatter,
            $departmentItemFormatter::getKey() => $departmentItemFormatter,
            $categoryItemFormatter::getKey() => $categoryItemFormatter,
            $productItemFormatter::getKey() => $productItemFormatter
        ];
    }

    /**
     * @param UrlData $urlData
     * @return array
     */
    public function buildBreadCrumbsByUrlData($urlData): array
    {
        $breadCrumbs = [];

        if ($urlData === null) {
            return $breadCrumbs;
        }

        $formatters = $this->getFormatters($urlData);
        foreach ($formatters as $formatter) {
            $breadCrumbs[] = [
                'name' => $formatter->getName($urlData),
                'url' => $formatter->getUrl($urlData)
            ];
        }

        return $breadCrumbs;
    }

    /**
     * @param UrlData $urlData
     * @return BreadCrumbsItemFormatterInterface[]
     */
    public function getFormatters(UrlData $urlData)
    {
        $formatters = [];

        $urlTypeId = $urlData->getUrlTypeId();
        switch ($urlTypeId) {
            case UrlType::URL_TYPE_ID_DEPARTMENT:
                $formatters = [
                    $this->formatters[HomeItemFormatter::getKey()],
                    $this->formatters[DepartmentItemFormatter::getKey()]
                ];
                break;
            case UrlType::URL_TYPE_ID_CATEGORY:
                $formatters = [
                    $this->formatters[HomeItemFormatter::getKey()],
                    $this->formatters[DepartmentItemFormatter::getKey()],
                    $this->formatters[CategoryItemFormatter::getKey()]
                ];
                break;
            case UrlType::URL_TYPE_ID_PRODUCT:
                $formatters = [
                    $this->formatters[HomeItemFormatter::getKey()],
                    $this->formatters[DepartmentItemFormatter::getKey()],
                    $this->formatters[CategoryItemFormatter::getKey()],
                    $this->formatters[ProductItemFormatter::getKey()]
                ];
        }

        return $formatters;
    }
}
