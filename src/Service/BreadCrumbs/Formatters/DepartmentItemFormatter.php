<?php

namespace App\Service\BreadCrumbs\Formatters;

use App\Repository\UrlsRepository;
use App\Repository\DepartmentsRepository;
use App\Service\Department\DepartmentService;
use App\Service\Url\UrlService;
use App\UrlData\UrlData;

class DepartmentItemFormatter implements BreadCrumbsItemFormatterInterface
{
    private const KEY = 'department';

    /** @var DepartmentService */
    private $departmentService;

    /** @var UrlsRepository */
    private $catalogUrlsRepository;

    public function __construct(
        DepartmentService $departmentService,
        UrlsRepository $catalogUrlsRepository
    ) {
        $this->departmentService = $departmentService;
        $this->catalogUrlsRepository = $catalogUrlsRepository;
    }

    public function getName(UrlData $urlData)
    {
        $department = $this->departmentService->getDepartmentEntryById($urlData->getDepartmentId());

        return $department[DepartmentsRepository::PROPERTY_NAME];
    }

    public function getUrl(UrlData $urlData)
    {
        $url = $this->catalogUrlsRepository->getDepartmentUrlByDepartmentId($urlData->getDepartmentId());

        return UrlService::formatUri($url);
    }

    public static function getKey()
    {
        return self::KEY;
    }
}
