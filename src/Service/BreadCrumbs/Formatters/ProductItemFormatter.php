<?php

namespace App\Service\BreadCrumbs\Formatters;

use App\Repository\UrlsRepository;
use App\Service\Url\UrlService;
use App\UrlData\UrlData;

class ProductItemFormatter implements BreadCrumbsItemFormatterInterface
{
    private const KEY = 'product';

    /** @var UrlsRepository */
    private $urlsRepository;

    public function __construct(
        UrlsRepository $urlsRepository
    ) {
        $this->urlsRepository = $urlsRepository;
    }

    public function getName(UrlData $urlData)
    {
        return 'Статья #' . $urlData->getEntityId();
    }

    public function getUrl(UrlData $urlData)
    {
        $url = $this->urlsRepository->getProductUrl($urlData->getDepartmentId(), $urlData->getEntityId());
        return UrlService::formatUri($url);
    }

    public static function getKey(): string
    {
        return self::KEY;
    }
}
