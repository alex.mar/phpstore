<?php

namespace App\Service\BreadCrumbs\Formatters;

use App\UrlData\UrlData;

interface BreadCrumbsItemFormatterInterface
{
    public function getName(UrlData $urlData);

    public function getUrl(UrlData $urlData);

    public static function getKey();
}
