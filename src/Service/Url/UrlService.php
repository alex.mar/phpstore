<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 04.03.2019
 * Time: 16:30
 */

namespace App\Service\Url;

use App\Repository\UrlsRepository;
use Doctrine\DBAL\DBALException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UrlService
{
    public const PARAM_PROTOCOL = 'protocol';
    public const PARAM_DOMAIN = 'domain';

    /** @var ContainerInterface */
    private $container;

    /** @var UrlsRepository */
    private $catalogUrlsRepository;

    public function __construct(
        ContainerInterface $container,
        UrlsRepository $catalogUrlsRepository
    ) {
        $this->container = $container;
        $this->catalogUrlsRepository = $catalogUrlsRepository;
    }

    /**
     * @param string $url
     * @return string
     */
    public static function formatUri($url): string
    {
        if (empty(trim($url, '/'))) {
            return $url;
        }
        return '/' . trim($url, '/') . '/';
    }

    public static function formatTableUri($url): string
    {
        return trim($url, '/') . '/';
    }

    /**
     * @return string
     */
    public function getDomainUrl(): string
    {
        return $this->container->getParameter(self::PARAM_PROTOCOL)
            . $this->container->getParameter(self::PARAM_DOMAIN);
    }

    /**
     * @param $urls
     * @throws DBALException
     */
    public function setCatalogUrls($urls): void
    {
        $this->catalogUrlsRepository->setUrls($urls);
    }

    /**
     * @param int $urlTypeId
     * @param string $hash
     * @throws DBALException
     */
    public function deleteUnusedCatalogUrls($urlTypeId, $hash): void
    {
        $this->catalogUrlsRepository->deleteUrlsByUrlTypeAndNotHash($urlTypeId, $hash);
    }

    /**
     * @param int $urlTypeId
     * @param string $hash
     * @return false|mixed
     * @throws DBALException
     */
    public function getCountUnusedCatalogUrls($urlTypeId, $hash)
    {
        return $this->catalogUrlsRepository->getCountUrlsByUrlTypeAndNotHash($urlTypeId, $hash);
    }
}
