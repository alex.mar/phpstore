<?php

namespace App\Service\Validator;

class ValidatorService
{
    public static function validateTags(string $string): string
    {
        return htmlspecialchars(strip_tags($string));
    }
}
