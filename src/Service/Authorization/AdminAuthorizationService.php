<?php

namespace App\Service\Authorization;

use App\Repository\AdminUsersRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 07.01.2019
 * Time: 10:20
 */

class AdminAuthorizationService
{
    const SESSION_USER_KEY = 'adminSessionUserKey';
    const SESSION_USER_ID = 'adminSessionUserId';

    /** @var SessionInterface */
    private $session;

    /** @var RouterInterface */
    private $router;

    /** @var AdminUsersRepository */
    private $adminUsersRepository;

    public function __construct(
        SessionInterface $session,
        RouterInterface $router,
        AdminUsersRepository $adminUsersRepository
    ) {
        $this->session = $session;
        $this->router = $router;
        $this->adminUsersRepository = $adminUsersRepository;
    }

    public function checkAuth()
    {
        $sessionKey = $this->session->get(self::SESSION_USER_KEY);
        $userId = $this->session->get(self::SESSION_USER_ID);
        if (!$this->checkAuthUser($userId, $sessionKey)) {
            $redirect = new RedirectResponse($this->router->generate('admin_login'));
            $redirect->send();
            die();
        }
    }

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function checkIssetUser(string $email, string $password)
    {
        $password = md5($password);

        $userId = $this->adminUsersRepository->getIdByEmailAndPassword($email, $password);

        if (empty($userId)) {
            return false;
        }

        return $userId;
    }

    /**
     * @param int $userId
     * @param string $key
     */
    public function setSessionKey(int $userId, string $key)
    {
        $this->adminUsersRepository->updateSessionKeyByUserId($userId, $key);
    }

    /**
     * @param mixed $userId
     * @param mixed $sessionKey
     * @return bool
     */
    private function checkAuthUser($userId, $sessionKey)
    {
        if (empty($sessionKey) || empty($userId)) {
            return false;
        }

        $sessionKeyFromDB = $this->adminUsersRepository->getSessionKeyByUserId($userId);

        if ($sessionKey !== $sessionKeyFromDB) {
            return false;
        }

        return true;
    }
}