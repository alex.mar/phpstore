<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 10.03.2019
 * Time: 14:36
 */

namespace App\Service\StaticPage;

use App\Repository\StaticPagesRepository;
use App\Service\Url\UrlService;

class StaticPageService
{
    /** @var StaticPagesRepository */
    private $staticPagesRepository;

    /** @var UrlService */
    private $urlService;

    /** @var array  */
    private $staticPageEntries = [];

    public function __construct(
        StaticPagesRepository $staticPagesRepository,
        UrlService $urlService
    ) {
        $this->staticPagesRepository = $staticPagesRepository;
        $this->urlService = $urlService;

        $this->init();
    }

    private function init()
    {
        if (empty($this->staticPageEntries)) {
            $staticPages = $this->staticPagesRepository->getAllStaticPages();
            foreach ($staticPages as $staticPage) {
                $this->staticPageEntries[$staticPage[StaticPagesRepository::FIELD_ID]] = $staticPage;
            }
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function getStaticPageEntryById($id)
    {
        if (isset($this->staticPageEntries[$id])) {
            return $this->staticPageEntries[$id];
        }

        return [];
    }

    /**
     * @return array
     */
    public function getAllStaticPages()
    {
        return $this->staticPageEntries;
    }

    /**
     * @param $staticPageData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setStaticPage($staticPageData)
    {
        $this->staticPagesRepository->setStaticPage($staticPageData);
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteStaticPageById($id)
    {
        $this->staticPagesRepository->deleteById($id);
    }
}
