<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 19.03.2019
 * Time: 23:09
 */

namespace App\Service\ProductLoader;

use App\Service\ProductLoader\Loaders\ArticleProductsLoader;
use App\Service\ProductLoader\Loaders\EmptyProductsLoader;
use App\Service\ProductLoader\Loaders\ProductLoaderInterface;

class ProductLoaderFactory
{
    const PRODUCT_TYPE_ARTICLE = 'article';
    const PRODUCT_TYPE_BOOK = 'book';

    /** @var ProductLoaderInterface[] */
    private $loaders = [];

    /**
     * ProductLoaderFactory constructor.
     * @param ArticleProductsLoader $articleProductsLoader
     */
    public function __construct(
        ArticleProductsLoader $articleProductsLoader
    ) {
        $this->loaders = [
            $articleProductsLoader
        ];
    }

    /**
     * @param $departmentId
     * @return ProductLoaderInterface
     */
    public function buildByDepartmentId($departmentId)
    {
        foreach ($this->loaders as $loader) {
            if ($loader->isDependDepartment($departmentId)) {
                return $loader;
            }
        }

        return new EmptyProductsLoader();
    }
}