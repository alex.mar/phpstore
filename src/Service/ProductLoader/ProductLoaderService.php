<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 23.03.2019
 * Time: 11:49
 */

namespace App\Service\ProductLoader;

use App\Service\Url\UrlService;

class ProductLoaderService
{
    /** @var UrlService */
    private $urlService;

    public function __construct(UrlService $urlService)
    {
        $this->urlService = $urlService;
    }

    /**
     * @return string
     */
    public static function getProductLoadersNamespace()
    {
        return __NAMESPACE__ . '\\Loaders';
    }

    /**
     * @param $text
     * @param int $limit
     * @return string
     */
    public static function getShortText($text, $limit = 500): string
    {
        $clearText = strip_tags(html_entity_decode($text));
        $clearText = mb_substr($clearText, 0, $limit);
        $clearTextArray = explode(' ', $clearText);
        array_pop($clearTextArray);
        $uniqueWords = array_unique($clearTextArray);
        $duplicateWords = array_diff_key($clearTextArray, $uniqueWords);
        $clearTextArray = array_reverse($clearTextArray);
        $lastWord = '';
        foreach ($clearTextArray as $word) {
            if (!in_array($word, $duplicateWords)) {
                $lastWord = $word;
                break;
            }
        }
        $subText = false;
        if ($lastWord !== '') {
            $subText = strstr($text, $lastWord, true);
        }
        if ($subText !== false) {
            $text = $subText . '...';
        } else {
            $text = mb_substr($text, 0, 800)
                . '...';
        }

        return $text;
    }

    public static function getDecodeText($text): string
    {
        $text = htmlspecialchars_decode($text);
        $text = htmlspecialchars_decode($text);

        return $text;
    }

    public function applyProductImages($product)
    {
        $images = $product['images'];
        foreach ($images as $image) {
            $imagePath = $this->urlService->getDomainUrl() . '/' . ltrim($image['path'], '/');
            $position = $image['position'];
            $patternPath = '/{{img-' . $position . '}}/i';
            $patternAlt = '/{{alt-' . $position . '}}/i';
            $product['text'] = preg_replace($patternPath, $imagePath, $product['text']);
            $product['text'] = preg_replace($patternAlt, $image['alt'], $product['text']);
        }

        return $product;
    }
}
