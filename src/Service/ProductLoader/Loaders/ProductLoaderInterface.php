<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 19.03.2019
 * Time: 23:13
 */

namespace App\Service\ProductLoader\Loaders;

interface ProductLoaderInterface
{
    /**
     * @param int $productId
     * @return array
     */
    public function getProductById($productId);

    /**
     * @param array $productIds
     * @return array
     */
    public function getProductsByIds($productIds);

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getProducts($offset = 0, $limit = 0);

    /**
     * @param int $categoryId
     * @return array
     */
    public function getProductsByCategoryId($categoryId);

    /**
     * @param array $categoryIds
     * @param $sort
     * @return array
     */
    public function getProductsByCategoryIds($categoryIds, $sort);

    /**
     * @return string
     */
    public function getAdminProductTemplate();

    /**
     * @param array $product
     */
    public function setProduct($product);

    /**
     * @param string $departmentId
     * @return bool
     */
    public function isDependDepartment($departmentId);

    public function deleteByProductId($productId);
}
