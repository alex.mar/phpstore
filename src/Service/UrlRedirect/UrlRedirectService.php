<?php

namespace App\Service\UrlRedirect;

use App\Entity\Urls;
use App\Entity\UrlsRedirect;
use App\Repository\UrlsRedirectRepository;
use App\Repository\UrlsRepository;
use App\Service\Url\UrlService;

class UrlRedirectService
{
    /** @var UrlsRedirectRepository */
    private $urlsRedirectRepository;

    /** @var UrlsRepository */
    private $urlsRepository;

    public function __construct(
        UrlsRedirectRepository $urlsRedirectRepository,
        UrlsRepository $urlsRepository
    ) {
        $this->urlsRedirectRepository = $urlsRedirectRepository;
        $this->urlsRepository = $urlsRepository;
    }

    /**
     * @param $fromUrl
     * @return bool|string
     */
    public function getRedirectUrl($fromUrl)
    {
        $redirectUrl = false;

        $fromUrl = UrlService::formatTableUri($fromUrl);
        $urlRedirect = $this->urlsRedirectRepository->findOneBy([UrlsRedirect::FIELD_FROM => $fromUrl]);
        if ($urlRedirect instanceof UrlsRedirect) {
            $toUrl = $urlRedirect->getTo();
            $urlEntity = $this->urlsRepository->findOneBy([Urls::FIELD_URL => $toUrl]);
            if ($urlEntity instanceof Urls) {
                $redirectUrl = $toUrl;
            }
        }

        return UrlService::formatUri($redirectUrl);
    }
}
