<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 26.01.2019
 * Time: 14:20
 */

namespace App\Service\DepartmentCategory;

use App\Repository\CategoriesRepository;
use App\Repository\DepartmentCategoriesRepository;
use App\Repository\DepartmentsRepository;

class DepartmentCategoriesService
{
    const KEY_DEPARTMENTS = 'departments';
    const KEY_CATEGORIES = 'categories';
    const KEY_CATEGORY_NAME = 'categoryName';
    const KEY_DEPARTMENT_NAME  = 'departmentName';

    /** @var DepartmentCategoriesRepository */
    private $departmentCategoriesRepository;

    private $categoriesRelations = [];

    private $departmentRelations = [];

    public function __construct(
        DepartmentCategoriesRepository $departmentCategoriesRepository
    ) {
        $this->departmentCategoriesRepository = $departmentCategoriesRepository;
    }

    /**
     * @return array
     */
    public function getCategoryDepartmentRelations()
    {
        if (!empty($this->categoriesRelations)) {
            return $this->categoriesRelations;
        }

        $relations = $this->departmentCategoriesRepository->getAllRelations();

        foreach ($relations as $item) {
            if (!isset($this->categoriesRelations[$item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID]])) {
                $this->categoriesRelations[$item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID]] = [
                    CategoriesRepository::FIELD_ID => $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID],
                    CategoriesRepository::FIELD_NAME => $item[self::KEY_CATEGORY_NAME],
                    self::KEY_DEPARTMENTS => [
                        $item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID] => [
                            DepartmentsRepository::PROPERTY_ID
                            => $item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID],
                            DepartmentsRepository::PROPERTY_NAME => $item[self::KEY_DEPARTMENT_NAME]
                        ]
                    ]
                ];
            } else {
                $this->categoriesRelations[$item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID]]
                [self::KEY_DEPARTMENTS][$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]] = [
                    DepartmentsRepository::PROPERTY_ID => $item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID],
                    DepartmentsRepository::PROPERTY_NAME => $item[self::KEY_DEPARTMENT_NAME]
                ];
            }
        }

        return $this->categoriesRelations;
    }

    public function getDepartmentCategoryRelations()
    {
        if (!empty($this->departmentRelations)) {
            return $this->departmentRelations;
        }

        $relations = $this->departmentCategoriesRepository->getAllRelations();

        foreach ($relations as $item) {
            if (!isset($this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]])) {
                $this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]] = [
                    DepartmentsRepository::FIELD_ID => $item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID],
                    DepartmentsRepository::PROPERTY_NAME => $item[self::KEY_DEPARTMENT_NAME],
                    self::KEY_CATEGORIES => [
                        $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID] => [
                            CategoriesRepository::FIELD_ID
                            => $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID],
                            CategoriesRepository::FIELD_NAME => $item[self::KEY_CATEGORY_NAME]
                        ]
                    ]
                ];
            } else {
                $this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]]
                [self::KEY_CATEGORIES][$item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID]] = [
                    CategoriesRepository::FIELD_ID => $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID],
                    CategoriesRepository::FIELD_NAME => $item[self::KEY_CATEGORY_NAME]
                ];
            }
        }

        return $this->departmentRelations;
    }

    public function getDepartmentRelationsByDepartmentId($departmentId)
    {
        if (isset($this->departmentRelations[$departmentId])) {
            return $this->departmentRelations[$departmentId];
        }

        $relations = $this->departmentCategoriesRepository->getRelationsByDepartmentId($departmentId);

        if (empty($relations)) {
            return [];
        }

        foreach ($relations as $item) {
            if (!isset($this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]])) {
                $this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]] = [
                    DepartmentsRepository::FIELD_ID => $item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID],
                    DepartmentsRepository::PROPERTY_NAME => $item[self::KEY_DEPARTMENT_NAME],
                    self::KEY_CATEGORIES => [
                        $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID] => [
                            CategoriesRepository::FIELD_ID
                            => $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID],
                            CategoriesRepository::FIELD_NAME => $item[self::KEY_CATEGORY_NAME]
                        ]
                    ]
                ];
            } else {
                $this->departmentRelations[$item[DepartmentCategoriesRepository::PROPERTY_DEPARTMENT_ID]]
                [self::KEY_CATEGORIES][$item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID]] = [
                    CategoriesRepository::FIELD_ID => $item[DepartmentCategoriesRepository::PROPERTY_CATEGORY_ID],
                    CategoriesRepository::FIELD_NAME => $item[self::KEY_CATEGORY_NAME]
                ];
            }
        }

        return $this->departmentRelations[$departmentId];
    }

    /**
     * @param int $categoryId
     * @return array
     */
    public function getRelationsByCategoryId($categoryId)
    {
        if (isset($this->categoriesRelations[$categoryId])) {
            return $this->categoriesRelations[$categoryId];
        }

        $categoryDepartmentEntries = $this->getCategoryDepartmentRelations();
        return isset($categoryDepartmentEntries[$categoryId]) ? $categoryDepartmentEntries[$categoryId] : [];
    }

    /**
     * @param $categoryId
     * @param $departmentIds
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setRelationsByCategory($categoryId, $departmentIds)
    {
        $categoryRelations = $this->getRelationsByCategoryId($categoryId);
        if (empty($categoryRelations)) {
            $this->departmentCategoriesRepository->addRelationsByCategory($categoryId, $departmentIds);
        } else {
            $forInsert = array_diff($departmentIds, array_keys($categoryRelations[self::KEY_DEPARTMENTS]));
            $forDelete = array_diff(array_keys($categoryRelations[self::KEY_DEPARTMENTS]), $departmentIds);
            if (!empty($forInsert)) {
                $this->departmentCategoriesRepository->addRelationsByCategory($categoryId, $forInsert);
            }
            if (!empty($forDelete)) {
                $this->departmentCategoriesRepository->removeRelationsByCategory($categoryId, $forDelete);
            }
        }
    }

    /**
     * @param $relationId
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteRelationById($relationId)
    {
        $this->departmentCategoriesRepository->deleteRelationById($relationId);
    }
}