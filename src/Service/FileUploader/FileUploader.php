<?php

namespace App\Service\FileUploader;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    const IMG_PATH = 'images/';

    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function uploadFile(UploadedFile $file)
    {
        $fileName = $file->getClientOriginalName();
//        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
        }

        return $fileName;
    }

    /**
     * @param UploadedFile[] $files
     */
    public function uploadFiles($files)
    {
        foreach ($files as $file) {
            $this->uploadFile($file);
        }
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    public function correctImgPath()
    {
        if (strpos($this->targetDirectory, self::IMG_PATH) === false) {
            $this->targetDirectory = self::IMG_PATH . $this->targetDirectory;
        }
    }
}
