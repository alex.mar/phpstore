<?php

namespace App\Service\SitemapGenerator;

interface SitemapGeneratorInterface
{
    public function run();
}
