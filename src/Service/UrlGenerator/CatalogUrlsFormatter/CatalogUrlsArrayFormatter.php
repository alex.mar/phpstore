<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 22.03.2019
 * Time: 21:21
 */

namespace App\Service\UrlGenerator\CatalogUrlsFormatter;

use App\Repository\UrlsRepository;

class CatalogUrlsArrayFormatter implements CatalogUrlFormatterInterface
{
    /**
     * @param array $data
     * @return array
     */
    public static function format(array $data)
    {
        $result = [];

        foreach ($data as $item) {
            $result[] = [
                UrlsRepository::FIELD_URL => $item[UrlsRepository::FIELD_URL] ?? '',
                UrlsRepository::FIELD_OPEN => (int) $item[UrlsRepository::FIELD_OPEN] ?? 0,
                UrlsRepository::PROPERTY_URL_TYPE_ID => (int) $item[UrlsRepository::PROPERTY_URL_TYPE_ID] ?? 0,
                UrlsRepository::PROPERTY_DEPARTMENT_ID => (int) $item[UrlsRepository::PROPERTY_DEPARTMENT_ID] ?? 0,
                UrlsRepository::PROPERTY_ENTITY_ID => (int) $item[UrlsRepository::PROPERTY_ENTITY_ID] ?? 0,
                UrlsRepository::FIELD_HASH => $item[UrlsRepository::FIELD_HASH] ?? ''
            ];
        }

        return $result;
    }
}
