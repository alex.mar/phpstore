<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 22.03.2019
 * Time: 21:20
 */

namespace App\Service\UrlGenerator\CatalogUrlsFormatter;

interface CatalogUrlFormatterInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public static function format(array $data);
}