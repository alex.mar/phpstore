<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 14.03.2019
 * Time: 22:44
 */

namespace App\Service\UrlGenerator\UrlLoaders;

use App\Service\UrlGenerator\UrlLoaders\Loaders\CategoryUrlLoader;
use App\Service\UrlGenerator\UrlLoaders\Loaders\DepartmentUrlLoader;
use App\Service\UrlGenerator\UrlLoaders\Loaders\ProductUrlLoader;
use App\Service\UrlGenerator\UrlLoaders\Loaders\StaticPageUrlLoader;
use App\Service\UrlGenerator\UrlLoaders\Loaders\UrlLoaderInterface;

class UrlLoaderFactory
{
    /** @var UrlLoaderInterface[] */
    private $urlLoaders = [];

    public function __construct(
        DepartmentUrlLoader $departmentUrlLoader,
        CategoryUrlLoader $categoryUrlLoader,
        StaticPageUrlLoader $staticPageUrlLoader,
        ProductUrlLoader $productUrlLoader
    ) {
        $this->urlLoaders = [
            $departmentUrlLoader,
            $categoryUrlLoader,
            $staticPageUrlLoader,
            $productUrlLoader
        ];
    }

    /**
     * @param array $urlTypes
     * @return UrlLoaderInterface[]
     */
    public function buildUrlLoaders(array $urlTypes)
    {
        $urlLoaders = [];

        $urlTypes = array_flip($urlTypes);
        /** @var UrlLoaderInterface $urlLoader */
        foreach ($this->urlLoaders as $urlLoader) {
            if (isset($urlTypes[$urlLoader->getLoaderType()])) {
                $urlLoaders[] = $urlLoader;
            }
        }

        return $urlLoaders;
    }
}