<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 14.03.2019
 * Time: 22:47
 */

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

interface UrlLoaderInterface
{
    /**
     * @return string
     */
    public function getLoaderType();

    public function process();
}
