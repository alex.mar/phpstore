<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 14.03.2019
 * Time: 22:58
 */

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

use App\Repository\UrlsRepository;
use App\Service\Department\DepartmentService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\Url\UrlService;
use App\Service\UrlGenerator\CatalogUrlsFormatter\CatalogUrlsArrayFormatter;
use App\Service\UrlGenerator\Logger\Logger;
use App\Service\UrlType\UrlTypeService;

class DepartmentUrlLoader extends AbstractUrlLoader
{
    const URL_TYPE = 'dep';
    const URL_PREFIX = 'dep';

    /** @var DepartmentService */
    private $departmentService;

    public function __construct(
        Logger $logger,
        UrlTypeService $urlTypeService,
        UrlService $urlService,
        ProductLoaderFactory $productLoaderFactory,
        DepartmentService $departmentService
    ) {
        parent::__construct($logger, $urlTypeService, $urlService, $productLoaderFactory);

        $this->departmentService = $departmentService;
    }

    /**
     * @return string
     */
    public function getLoaderType()
    {
        return self::URL_TYPE;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function process()
    {
        $this->logger->writeln($this->getStartLogString());

        $departments = $this->departmentService->getAllDepartments();

        $hash = $this->generateHash();

        $resultUrls = [];
        $countOpened = 0;
        $countClosed = 0;
        foreach ($departments as $departmentId => $department) {
            $resultUrls[$departmentId] = $department;
            $resultUrls[$departmentId][UrlsRepository::PROPERTY_DEPARTMENT_ID] = $departmentId;
            $resultUrls[$departmentId][UrlsRepository::PROPERTY_ENTITY_ID] = $departmentId;
            $resultUrls[$departmentId][UrlsRepository::PROPERTY_URL_TYPE_ID] = $this->urlTypeId;
            $productLoader = $this->productLoaderFactory->buildByDepartmentId($departmentId);
            $products = $productLoader->getProducts(0, 1);
            if (empty($products)) {
                $resultUrls[$departmentId][UrlsRepository::FIELD_OPEN]
                    = UrlsRepository::VALUE_OPEN_NO;
                ++$countClosed;
            } else {
                $resultUrls[$departmentId][UrlsRepository::FIELD_OPEN]
                    = UrlsRepository::VALUE_OPEN_YES;
                ++$countOpened;
            }
            $resultUrls[$departmentId][UrlsRepository::FIELD_HASH] = $hash;
        }

        $resultUrls = $this->buildUrls($resultUrls);
        $resultUrls = CatalogUrlsArrayFormatter::format($resultUrls);
        $resultUrls = $this->filterUrls($resultUrls);

        if (empty($resultUrls)) {
            return;
        }
        $countUrls = count($resultUrls);
        $this->urlService->setCatalogUrls($resultUrls);

        $countDeleted = $this->urlService->getCountUnusedCatalogUrls($this->urlTypeId, $hash);
        $this->urlService->deleteUnusedCatalogUrls($this->urlTypeId, $hash);

        $this->logger->writeln([
            $this->getFinishedLogString($countUrls, $countOpened, $countClosed, $countDeleted),
            ''
        ]);
    }

    protected function buildUrls(array $entities)
    {
        foreach ($entities as $key => $entity) {
            $entities[$key][UrlsRepository::FIELD_URL]
                = self::URL_PREFIX . '/' . $entity[UrlsRepository::FIELD_URL];
        }

        return $entities;
    }
}
