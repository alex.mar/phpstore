<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 30.03.2019
 * Time: 16:48
 */

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

use App\Entity\Categories;
use App\Entity\Departments;
use App\Repository\UrlsRepository;
use App\Repository\CategoriesMetaRepository;
use App\Service\Category\CategoriesService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\Url\UrlService;
use App\Service\UrlGenerator\CatalogUrlsFormatter\CatalogUrlsArrayFormatter;
use App\Service\UrlGenerator\Logger\Logger;
use App\Service\UrlGenerator\UrlStringBuilder\UrlStringBuilder;
use App\Service\UrlType\UrlTypeService;

class CategoryUrlLoader extends AbstractUrlLoader
{
    const URL_TYPE = 'cat';
    const URL_PREFIX = 'cat';

    /** @var CategoriesService */
    private $categoriesService;

    /** @var UrlStringBuilder */
    private $urlStringBulder;

    public function __construct(
        Logger $logger,
        UrlTypeService $urlTypeService,
        UrlService $urlService,
        ProductLoaderFactory $productLoaderFactory,
        CategoriesService $categoriesService,
        UrlStringBuilder $urlStringBuilder
    ) {
        parent::__construct($logger, $urlTypeService, $urlService, $productLoaderFactory);

        $this->categoriesService = $categoriesService;
        $this->urlStringBulder = $urlStringBuilder;
    }

    public function getLoaderType()
    {
        return self::URL_TYPE;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function process()
    {
        $this->logger->writeln($this->getStartLogString());

        $categories = $this->categoriesService->getAllCategories();

        $hash = $this->generateHash();

        $resultUrls = [];
        $countOpened = 0;
        $countClosed = 0;
        $index = 0;
        foreach ($categories as $categoryId => $category) {
            $categoryRelations = $this->categoriesService->getCategoryRelations($categoryId);
            $relations = $categoryRelations[$categoryId][CategoriesService::KEY_DEPARTMENTS];
            foreach ($relations as $departmentId => $relation) {
                $resultUrls[$index] = $category;
                $resultUrls[$index][UrlsRepository::PROPERTY_DEPARTMENT_ID] = $departmentId;
                $resultUrls[$index][UrlsRepository::PROPERTY_URL_TYPE_ID] = $this->urlTypeId;
                $resultUrls[$index][UrlsRepository::PROPERTY_ENTITY_ID] = $categoryId;
                $resultUrls[$index][CategoriesMetaRepository::FIELD_TITLE]
                    = $relation[CategoriesMetaRepository::FIELD_TITLE];
                $resultUrls[$index]['departmentUrl'] = $relation[Departments::FIELD_URL];

                $productLoader = $this->productLoaderFactory->buildByDepartmentId($departmentId);
                $products = $productLoader->getProductsByCategoryId($categoryId);
                if (empty($products)) {
                    $resultUrls[$index][UrlsRepository::FIELD_OPEN] = UrlsRepository::VALUE_OPEN_NO;
                    ++$countClosed;
                } else {
                    $isOpen = false;
                    foreach ($products as $product) {
                        if ($product['status'] === 'published') {
                            $resultUrls[$index][UrlsRepository::FIELD_OPEN] = UrlsRepository::VALUE_OPEN_YES;
                            ++$countOpened;
                            $isOpen = true;
                            break;
                        }
                    }
                    if (!$isOpen) {
                        $resultUrls[$index][UrlsRepository::FIELD_OPEN] = UrlsRepository::VALUE_OPEN_NO;
                        ++$countClosed;
                    }
                }
                $resultUrls[$index][UrlsRepository::FIELD_HASH] = $hash;

                ++$index;
            }
        }

        $resultUrls = $this->buildUrls($resultUrls);
        $resultUrls = CatalogUrlsArrayFormatter::format($resultUrls);
        $resultUrls = $this->filterUrls($resultUrls);

        if (empty($resultUrls)) {
            return;
        }
        $countUrls = count($resultUrls);
        $this->urlService->setCatalogUrls($resultUrls);

        $countDeleted = $this->urlService->getCountUnusedCatalogUrls($this->urlTypeId, $hash);
        $this->urlService->deleteUnusedCatalogUrls($this->urlTypeId, $hash);

        $this->logger->writeln([
            $this->getFinishedLogString($countUrls, $countOpened, $countClosed, $countDeleted),
            ''
        ]);

        return;
    }

    public function buildUrls(array $entities)
    {
        foreach ($entities as $key => $entity) {
            $entities[$key][UrlsRepository::FIELD_URL]
                = self::URL_PREFIX . '/'
                . rtrim($entity['departmentUrl'], '/') . '/'
                . $this->urlStringBulder->buildUrl($entity[Categories::FIELD_NAME]);
        }

        return $entities;
    }
}
