<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 22.03.2019
 * Time: 21:32
 */

namespace App\Service\UrlGenerator\UrlLoaders\Loaders;

use App\Repository\UrlsRepository;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\Url\UrlService;
use App\Service\UrlGenerator\Logger\Logger;
use App\Service\UrlType\UrlTypeService;

abstract class AbstractUrlLoader implements UrlLoaderInterface
{
    /** @var Logger */
    protected $logger;

    /** @var UrlTypeService */
    protected $urlTypeService;

    /** @var UrlService */
    protected $urlService;

    /** @var ProductLoaderFactory */
    protected $productLoaderFactory;

    /** @var int */
    protected $urlTypeId;

    public function __construct(
        Logger $logger,
        UrlTypeService $urlTypeService,
        UrlService $urlService,
        ProductLoaderFactory $productLoaderFactory
    ) {
        $this->logger = $logger;
        $this->urlTypeService = $urlTypeService;
        $this->urlService = $urlService;
        $this->productLoaderFactory = $productLoaderFactory;

        $this->init();
    }

    protected function init()
    {
        $this->urlTypeId = $this->urlTypeService->getUrlTypeIdByUrlType($this->getLoaderType());
    }

    abstract protected function buildUrls(array $entities);

    /**
     * @param array $urls
     * @return array
     */
    protected function filterUrls($urls)
    {
        foreach ($urls as $key => $item) {
            if (!isset($item[UrlsRepository::FIELD_URL])
                || !isset($item[UrlsRepository::PROPERTY_URL_TYPE_ID])
                || !isset($item[UrlsRepository::PROPERTY_DEPARTMENT_ID])
                || !isset($item[UrlsRepository::PROPERTY_ENTITY_ID])
                || !isset($item[UrlsRepository::FIELD_HASH])) {
                unset($urls[$key]);
            }
        }

        return $urls;
    }

    /**
     * @return string
     */
    protected function generateHash()
    {
        return md5($this->getLoaderType() . mt_rand());
    }

    protected function getStartLogString()
    {
        return 'Start: ' . $this->getLoaderType();
    }

    protected function getFinishedLogString($count, $opened, $closed, $deleted)
    {
        return 'Finished: '
            . $count . ' generated | '
            . $opened . ' opened | '
            . $closed . ' closed | '
            . $deleted . ' deleted';
    }
}
