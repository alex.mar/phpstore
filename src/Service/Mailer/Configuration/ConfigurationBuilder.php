<?php

namespace App\Service\Mailer\Configuration;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConfigurationBuilder
{
    /**
     * @param ParameterBagInterface $params
     * @return Configuration
     */
    public function buildNoReply(ParameterBagInterface $params): Configuration
    {
        return new Configuration(
            $params->get('mailer_transport'),
            $params->get('mailer_host'),
            $params->get('mailer_auth_mode'),
            $params->get('mailer_port'),
            $params->get('mailer_encryption'),
            $params->get('mailer_username_no_reply'),
            $params->get('mailer_password_no_reply')
        );
    }
}
