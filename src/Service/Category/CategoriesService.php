<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 11.01.2019
 * Time: 23:08
 */

namespace App\Service\Category;

use App\Entity\DepartmentCategories;
use App\Repository\CategoriesRepository;
use App\Repository\DepartmentCategoriesRepository;
use App\Repository\DepartmentsRepository;
use App\Repository\CategoriesMetaRepository;

class CategoriesService
{
    const KEY_DEPARTMENTS = 'departments';

    /** @var CategoriesRepository */
    private $categoriesRepository;

    /** @var DepartmentCategoriesRepository */
    private $departmentCategoriesRepository;

    /** @var CategoriesMetaRepository */
    private $metaCategoriesRepository;

    /** @var array  */
    private $categories = [];

    public function __construct(
        CategoriesRepository $categoriesRepository,
        DepartmentCategoriesRepository $departmentCategoriesRepository,
        CategoriesMetaRepository $metaCategoriesRepository
    ) {
        $this->categoriesRepository = $categoriesRepository;
        $this->departmentCategoriesRepository = $departmentCategoriesRepository;
        $this->metaCategoriesRepository = $metaCategoriesRepository;

        $this->initCategories();
    }

    private function initCategories()
    {
        $categories = $this->categoriesRepository->getAllCategories();

        foreach ($categories as $category) {
            $this->categories[$category[CategoriesRepository::FIELD_ID]] = $category;
        }
    }

    /**
     * @return array
     */
    public function getAllCategories()
    {
        return $this->categories;
    }

    /**
     * @param $id
     * @return array
     */
    public function getCategoryEntryById($id)
    {
        if (isset($this->categories[$id])) {
            return $this->categories[$id];
        }

        return [];
    }

    /**
     * @param array $category
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    public function setCategory($category)
    {
        $categoryId = $this->categoriesRepository->setCategory($category);
        $this->departmentCategoriesRepository->addRelationsByCategory(
            $categoryId,
            array_keys($category[self::KEY_DEPARTMENTS])
        );
        /** @var DepartmentCategories[] $relations */
        $relations = $this->departmentCategoriesRepository->getRelationsByCategoryIdAndDepartmentIds(
            $categoryId,
            array_keys($category[self::KEY_DEPARTMENTS])
        );
        foreach ($relations as $relation) {
            $metaData = $category[self::KEY_DEPARTMENTS][$relation->getDepartmentId()];
            $this->metaCategoriesRepository->setMetaByRelationId($relation->getId(), $metaData);
        }
    }

    /**
     * @param $name
     * @return int
     */
    public function getCategoryIdByName($name)
    {
        $names = array_column(
            $this->categories,
            CategoriesRepository::FIELD_NAME,
            CategoriesRepository::FIELD_ID
        );
        $names = array_change_key_case(array_flip($names));

        if (isset($names[strtolower($name)])) {
            return $names[strtolower($name)];
        }

        return 0;
    }

    /**
     * @param int $id
     * @param string $name
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateNameById($id, $name)
    {
        $this->categoriesRepository->updateNameById($id, $name);
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteCategoryById($id)
    {
        $this->categoriesRepository->deleteById($id);
    }

    /**
     * @param int $categoryId
     * @return array
     */
    public function getCategoryRelations($categoryId)
    {
        $result = [];

        $relations = $this->categoriesRepository->getCategoryRelations($categoryId);
        foreach ($relations as $relation) {
            $result[$categoryId][self::KEY_DEPARTMENTS][$relation[DepartmentsRepository::FIELD_ID]] = $relation;
        }

        return $result;
    }
}
