<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 07.01.2019
 * Time: 20:56
 */

namespace App\Service\User;

class User
{
    /** @var bool */
    private $isAuth = false;

    /** @var string */
    private $name;

    /** @var string */
    private $logo;

    /**
     * @return bool
     */
    public function isAuth(): bool
    {
        return $this->isAuth;
    }

    /**
     * @param bool $isAuth
     * @return User
     */
    public function setIsAuth(bool $isAuth): User
    {
        $this->isAuth = $isAuth;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     * @return User
     */
    public function setLogo(string $logo): User
    {
        $this->logo = $logo;

        return $this;
    }
}