<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 28.12.2018
 * Time: 18:08
 */

namespace App\Service\Menu;

use App\Entity\Menu;
use App\Repository\MenuRepository;
use App\Repository\UrlsRepository;
use App\Service\Url\UrlService;
use App\UrlData\UrlData;

class MenuService implements MenuServiceInterface
{
    public const TWIG_KEY_NAME = 'name';
    public const TWIG_KEY_LINK = 'link';
    public const TWIG_KEY_ICON = 'icon';

    /** @var MenuRepository  */
    private $menuRepository;

    private $menu;

    public function __construct(
        MenuRepository $menuRepository
    ) {
        $this->menuRepository = $menuRepository;
    }

    public function loadMenu(UrlData $urlData)
    {
        if (!empty($this->menu)) {
            return $this->menu;
        }
        $menu = $this->menuRepository->getAllMenuItems();

        $result = [];
        $hasActive = false;
        /** @var \App\Entity\Menu $item */
        foreach ($menu as $item) {
            $active = false;
            if (!$hasActive && ($item->getLink() === $urlData->getUrl())) {
                $hasActive = true;
                $active = true;
            }
            $subItems = [];
            if ($item->getLevel() === 2) {
                $result[$item->getParentId()]['subItems'][] = [
                    self::TWIG_KEY_NAME => $item->getName(),
                    self::TWIG_KEY_LINK => UrlService::formatUri($item->getLink()),
                    self::TWIG_KEY_ICON => $item->getIcon(),
                    'active' => $active
                ];
            } else {
                $result[$item->getId()] = [
                    self::TWIG_KEY_NAME => $item->getName(),
                    self::TWIG_KEY_LINK => UrlService::formatUri($item->getLink()),
                    self::TWIG_KEY_ICON => $item->getIcon(),
                    'active' => $active,
                    'subItems' => $subItems
                ];
            }
        }

        $this->menu = $result;

        return $result;
    }

    /**
     * @return array
     */
    public function getAllMenuItems()
    {
        $result = [];

        $menu = $this->menuRepository->getAllMenuItems();
        foreach ($menu as $item) {
            $result[] = $item->toArray();
        }

        return $result;
    }

    public function getItemById($id)
    {
        $result = [];

        $item = $this->menuRepository->find($id);
        if ($item instanceof Menu) {
            $result = $item->toArray();
        }

        return $result;
    }

    /**
     * @param $menuOrders
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateOrderByIds($menuOrders)
    {
        $this->menuRepository->updateOrderByIds($menuOrders);
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteItemById($id)
    {
        $this->menuRepository->deleteById($id);
    }

    /**
     * @param $itemData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setMenuItem($itemData)
    {
        $this->menuRepository->setMenuItem($itemData);
    }
}