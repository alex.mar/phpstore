<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 28.12.2018
 * Time: 18:12
 */

namespace App\Service\Menu;

use App\Repository\AdminMenuRepository;
use App\Service\Url\UrlService;

class AdminMenuService
{
    const TWIG_KEY_NAME = 'name';
    const TWIG_KEY_LINK = 'link';
    const TWIG_KEY_ICON = 'icon';
    const TWIG_KEY_LEVEL = 'level';
    const TWIG_KEY_HAS_SUBMENU = 'hasSubmenu';

    /** @var AdminMenuRepository */
    private $adminMenuRepository;

    public function __construct(
        AdminMenuRepository $adminMenuRepository
    ) {
        $this->adminMenuRepository = $adminMenuRepository;
    }

    public function loadMenu()
    {
        $menu = $this->adminMenuRepository->getAllMenu();

        $result = [];
        /** @var \App\Entity\AdminMenu $item */
        foreach ($menu as $item) {
            $result[] = [
                self::TWIG_KEY_NAME => $item->getName(),
                self::TWIG_KEY_LINK => UrlService::formatUri($item->getLink()),
                self::TWIG_KEY_ICON => $item->getIcon(),
                self::TWIG_KEY_LEVEL => $item->getLevel(),
                self::TWIG_KEY_HAS_SUBMENU => $item->getHasSubmenu()
            ];
        }

        return $result;
    }
}