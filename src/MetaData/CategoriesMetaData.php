<?php

namespace App\MetaData;

interface CategoriesMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
}
