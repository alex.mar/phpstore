<?php

namespace App\MetaData;

interface ArticlesMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_CATEGORY_ID = 'categoryId';
    public const FIELD_H1 = 'h1';
}
