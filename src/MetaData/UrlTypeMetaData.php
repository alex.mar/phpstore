<?php

namespace App\MetaData;

interface UrlTypeMetaData
{
    public const URL_TYPE_DEPARTMENT = 'dep';
    public const URL_TYPE_CATEGORY = 'cat';
    public const URL_TYPE_STATIC_PAGE = 'static';
    public const URL_TYPE_PRODUCT = 'prod';

    public const URL_TYPE_ID_DEPARTMENT = 1;
    public const URL_TYPE_ID_CATEGORY = 2;
    public const URL_TYPE_ID_PRODUCT = 3;
    public const URL_TYPE_ID_STATIC_PAGE = 4;
}
