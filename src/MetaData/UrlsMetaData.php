<?php

namespace App\MetaData;

interface UrlsMetaData
{
    public const FIELD_ID = 'id';
    public const FIELD_URL = 'url';
    public const FIELD_ENTITY_ID = 'entityId';
    public const FIELD_OPEN = 'open';

    public const OPEN_YES = 1;
    public const OPEN_NO = 0;
}
