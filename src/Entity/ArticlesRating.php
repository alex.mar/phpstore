<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticlesRatingRepository")
 */
class ArticlesRating
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $articleId;

    /**
     * @ORM\Column(type="integer")
     */
    private $ratingUp;

    /**
     * @ORM\Column(type="integer")
     */
    private $ratingDown;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleId(): ?int
    {
        return $this->articleId;
    }

    public function setArticleId(int $articleId): self
    {
        $this->articleId = $articleId;

        return $this;
    }

    public function getRatingUp(): ?int
    {
        return $this->ratingUp;
    }

    public function setRatingUp(int $rating): self
    {
        $this->ratingUp = $rating;

        return $this;
    }

    public function getRatingDown(): ?int
    {
        return $this->ratingDown;
    }

    public function setRatingDown(int $rating): self
    {
        $this->ratingDown = $rating;

        return $this;
    }
}
