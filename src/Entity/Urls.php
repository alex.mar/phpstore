<?php

namespace App\Entity;

use App\MetaData\UrlsMetaData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlsRepository")
 */
class Urls implements UrlsMetaData
{
    public function __construct()
    {
        $this->setDefaultDateTime();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="smallint", length=1)
     */
    private $open;

    /**
     * @ORM\Column(type="integer")
     */
    private $urlTypeId;

    /**
     * @ORM\Column(type="integer")
     */
    private $departmentId;

    /**
     * @ORM\Column(type="integer")
     */
    private $entityId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_update;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * @param int $open
     * @return Urls
     */
    public function setOpen($open): self
    {
        $this->open = $open;

        return $this;
    }

    public function getUrlTypeId(): ?int
    {
        return $this->urlTypeId;
    }

    public function setUrlTypeId(int $urlTypeId): self
    {
        $this->urlTypeId = $urlTypeId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    /**
     * @param mixed $departmentId
     */
    public function setDepartmentId($departmentId): void
    {
        $this->departmentId = $departmentId;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return Urls
     */
    public function setEntityId($entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->last_update;
    }

    public function setLastUpdate(\DateTimeInterface $last_update): self
    {
        $this->last_update = $last_update;

        return $this;
    }

    public function setDefaultDateTime()
    {
        $this->last_update = new \DateTime();
    }
}
