<?php

namespace App\Entity;

use App\MetaData\UrlTypeMetaData;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlTypeRepository")
 */
class UrlType implements UrlTypeMetaData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $urlType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $displayName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrlType(): ?string
    {
        return $this->urlType;
    }

    public function setUrlType(string $urlType): self
    {
        $this->urlType = $urlType;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }
}
