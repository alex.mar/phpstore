<?php

namespace App\Helper\Walkers;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 07.01.2019
 * Time: 15:00
 */

class SqlWalkerCustom extends SqlWalker
{
    const CODE_EMPTY_ARRAY_PARAMETER = 1;

    private $conn;

    public function __construct($query, $parserResult, array $queryComponents)
    {
        parent::__construct($query, $parserResult, $queryComponents);
        $this->conn = $this->getConnection();
    }

    /**
     * @param $inputParam
     * @return string
     * @throws Query\QueryException
     */
    public function walkInputParameter($inputParam)
    {
        $param = $this->getQuery()->getParameter($inputParam->name);
        if (!$param instanceof Query\Parameter) {
            throw Query\QueryException::unknownParameter($inputParam->name);
        }
        $value = $param->getValue();
        if (is_array($value)) {
            if (!count($value)) {
                throw new Query\QueryException('Empty array', self::CODE_EMPTY_ARRAY_PARAMETER);
            }
        }
        // Remember about query cache before some changes
        return '::??'.md5('::??'.$inputParam->name);
    }

    /**
     * return class name with namespace
     * @return string
     */
    public static function getClassName()
    {
        return get_called_class();
    }
}