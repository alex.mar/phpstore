<?php

namespace App\UrlData;

interface UrlDataFactoryInterface
{
    public function build($url);
}
