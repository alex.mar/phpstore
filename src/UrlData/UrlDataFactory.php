<?php

namespace App\UrlData;

use App\Entity\UrlType;
use App\Repository\ArticlesRepository;
use App\Repository\UrlsRepository;
use App\Service\Url\UrlService;

class UrlDataFactory implements UrlDataFactoryInterface
{
    /** @var UrlsRepository */
    private $catalogUrlRepository;

    /** @var ArticlesRepository */
    private $articlesRepository;

    public function __construct(
        UrlsRepository $catalogUrlsRepository,
        ArticlesRepository $articlesRepository
    ) {
        $this->catalogUrlRepository = $catalogUrlsRepository;
        $this->articlesRepository = $articlesRepository;
    }

    public function build($url)
    {
        $url = UrlService::formatTableUri($url);
        $urlDataEntity = $this->catalogUrlRepository->findBy(['url' => $url]);

        if (empty($urlDataEntity)) {
            return new UrlData([]);
        }

        $urlDataEntity = array_shift($urlDataEntity);

        $urlDataArray = [
            UrlsRepository::FIELD_ID => $urlDataEntity->getId(),
            UrlsRepository::FIELD_URL => $urlDataEntity->getUrl(),
            UrlsRepository::FIELD_OPEN => $urlDataEntity->getOpen(),
            UrlsRepository::PROPERTY_URL_TYPE_ID => $urlDataEntity->getUrlTypeId(),
            UrlsRepository::PROPERTY_DEPARTMENT_ID => $urlDataEntity->getDepartmentId(),
            'categoryId' => 0,
            'productId' => 0,
            UrlsRepository::PROPERTY_ENTITY_ID => $urlDataEntity->getEntityId()
        ];
        if ($urlDataEntity->getUrlTypeId() === UrlType::URL_TYPE_ID_CATEGORY) {
            $urlDataArray['categoryId'] = $urlDataEntity->getEntityId();
        } elseif ($urlDataEntity->getUrlTypeId() === UrlType::URL_TYPE_ID_PRODUCT) {
            $productId = $urlDataEntity->getEntityId();
            $product = $this->articlesRepository->getArticleById($productId);
            $urlDataArray['categoryId'] = $product['categoryId'];
            $urlDataArray['productId'] = $urlDataEntity->getEntityId();
        }

        return new UrlData($urlDataArray);
    }
}
