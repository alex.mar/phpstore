<?php

namespace App\Repository;

use App\Entity\UserArticleVisits;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserArticleVisits|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserArticleVisits|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserArticleVisits[]    findAll()
 * @method UserArticleVisits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserArticleVisitsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserArticleVisits::class);
    }

    public function getIdByIpAndArticleId($ip, $articleId)
    {
        $qb = $this->createQueryBuilder('uav')
            ->select('uav.id')
            ->where('uav.ip = :ip')
            ->andWhere('uav.articleId = :articleId')
            ->setParameters([
                'ip' => $ip,
                'articleId' => $articleId
            ]);

        return (int) DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    /**
     * @param $ip
     * @param $articleId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUserArticleVisit($ip, $articleId): void
    {
        $userArticleVisit = new UserArticleVisits();
        $userArticleVisit->setIp($ip);
        $userArticleVisit->setArticleId($articleId);

        $this->_em->persist($userArticleVisit);
        $this->_em->flush();
    }
}
