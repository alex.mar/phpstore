<?php

namespace App\Repository;

use App\Entity\AdminMenu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdminMenu|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminMenu|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminMenu[]    findAll()
 * @method AdminMenu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminMenuRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdminMenu::class);
    }

    public function getAllMenu()
    {
        return $this->createQueryBuilder('am')
            ->select()
            ->orderBy('am.order')
            ->getQuery()
            ->getResult();
    }
}
