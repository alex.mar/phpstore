<?php

namespace App\Repository;

use App\Entity\SearchStatistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SearchStatistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchStatistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchStatistic[]    findAll()
 * @method SearchStatistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchStatisticRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SearchStatistic::class);
    }

    /**
     * @param $query
     * @param $date
     * @param $resultArticles
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setSearchQuery($query, $date, $resultArticles): void
    {
        $searchStatistic = new SearchStatistic();
        $searchStatistic->setQuery($query);
        $searchStatistic->setDate($date);
        $searchStatistic->setResultArticles($resultArticles);
        $this->_em->persist($searchStatistic);

        $this->_em->flush();
    }
}
