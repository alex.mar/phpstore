<?php

namespace App\Repository;

use App\Entity\Urls;
use App\Entity\UrlType;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Urls|null find($id, $lockMode = null, $lockVersion = null)
 * @method Urls|null findOneBy(array $criteria, array $orderBy = null)
 * @method Urls[]    findAll()
 * @method Urls[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlsRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';
    const FIELD_URL = 'url';
    const FIELD_SHOW = 'show';
    const FIELD_OPEN = 'open';
    const FIELD_HASH = 'hash';

    const PROPERTY_URL_TYPE_ID = 'urlTypeId';
    const PROPERTY_DEPARTMENT_ID = 'departmentId';
    const PROPERTY_ENTITY_ID = 'entityId';

    const VALUE_OPEN_YES = 1;
    const VALUE_OPEN_NO = 0;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Urls::class);
    }

    /**
     * @param $urls
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setUrls($urls)
    {
        $query = "INSERT INTO urls (`url`, `open`, `url_type_id`, `department_id`, `entity_id`, `hash`)
                  VALUES ";

        foreach ($urls as $url) {
            $query .= "('{$url[self::FIELD_URL]}', {$url[self::FIELD_OPEN]}, {$url[self::PROPERTY_URL_TYPE_ID]},
               {$url[self::PROPERTY_DEPARTMENT_ID]}, {$url[self::PROPERTY_ENTITY_ID]},'{$url[self::FIELD_HASH]}'),";
        }
        $query = substr($query, 0, -1);
        $query .= ' ON DUPLICATE KEY UPDATE `open` = VALUES(open), `hash` = VALUES(hash);';

        $this->_em->getConnection()->executeQuery($query);
    }

    /**
     * @param int $urlTypeId
     * @param string $hash
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteUrlsByUrlTypeAndNotHash($urlTypeId, $hash)
    {
        $query = 'DELETE FROM urls
                  WHERE url_type_id = :urlTypeId AND hash != :hash';

        $params = [
            UrlsRepository::PROPERTY_URL_TYPE_ID => $urlTypeId,
            UrlsRepository::FIELD_HASH => $hash
        ];

        $this->_em->getConnection()->executeQuery($query, $params);
    }

    /**
     * @param int $urlTypeId
     * @param string $hash
     * @return false|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCountUrlsByUrlTypeAndNotHash($urlTypeId, $hash)
    {
        $query = 'SELECT COUNT(`id`) as count FROM urls
                  WHERE url_type_id = :urlTypeId AND hash != :hash';

        $params = [
            UrlsRepository::PROPERTY_URL_TYPE_ID => $urlTypeId,
            UrlsRepository::FIELD_HASH => $hash
        ];

        $count = $this->_em->getConnection()->fetchColumn($query, $params);

        return $count;
    }

    public function getProductOpenUrlsByProductIds($departmentId, $productIds)
    {
        $qb = $this->createQueryBuilder('cu')
            ->select('cu.entityId, cu.url')
            ->where('cu.urlTypeId = :urlTypeId')
            ->andWhere('cu.departmentId = :departmentId')
            ->andWhere('cu.entityId IN (:entityIds)')
            ->andWhere('cu.open = :open')
            ->setParameters([
                'urlTypeId' => UrlType::URL_TYPE_ID_PRODUCT,
                'departmentId' => $departmentId,
                'entityIds' => $productIds,
                'open' => self::VALUE_OPEN_YES
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function getDepartmentUrlByDepartmentId($departmentId)
    {
        $qb = $this->createQueryBuilder('cu')
            ->select('cu.url')
            ->where('cu.urlTypeId = :urlTypeId')
            ->andWhere('cu.departmentId = :departmentId')
            ->setParameters([
                'urlTypeId' => UrlType::URL_TYPE_ID_DEPARTMENT,
                'departmentId' => $departmentId
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    public function getDepartmentCategoryUrl($departmentId, $categoryId)
    {
        $qb = $this->createQueryBuilder('cu')
            ->select('cu.url')
            ->where('cu.urlTypeId = :urlTypeId')
            ->andWhere('cu.departmentId = :departmentId')
            ->andWhere('cu.entityId = :entityId')
            ->setParameters([
                'urlTypeId' => UrlType::URL_TYPE_ID_CATEGORY,
                'departmentId' => $departmentId,
                'entityId' => $categoryId
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    public function getProductUrl($departmentId, $productId)
    {
        $qb = $this->createQueryBuilder('cu')
            ->select('cu.url')
            ->where('cu.urlTypeId = :urlTypeId')
            ->andWhere('cu.departmentId = :departmentId')
            ->andWhere('cu.entityId = :entityId')
            ->setParameters([
                'urlTypeId' => UrlType::URL_TYPE_ID_PRODUCT,
                'departmentId' => $departmentId,
                'entityId' => $productId
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    public function getCategoryUrls($categoryIds, $departmentId)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.entityId, u.url')
            ->where('u.departmentId = :departmentId')
            ->andWhere('u.urlTypeId = :urlTypeId')
            ->andWhere('u.entityId IN (:categoryIds)')
            ->andWhere('u.open = :open')
            ->setParameters([
                'departmentId' => $departmentId,
                'urlTypeId' => UrlType::URL_TYPE_ID_CATEGORY,
                'categoryIds' => $categoryIds,
                'open' => Urls::OPEN_YES
            ]);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll();
    }

    public function getOpenUrls(): array
    {
        return $this->findBy([Urls::FIELD_OPEN => Urls::OPEN_YES]);
    }
}
