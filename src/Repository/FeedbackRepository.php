<?php

namespace App\Repository;

use App\Entity\Feedback;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Feedback|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feedback|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feedback[]    findAll()
 * @method Feedback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Feedback::class);
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $text
     * @return int
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setFeedback($name, $email, $text): int
    {
        $feedback = new Feedback();
        $feedback->setName($name);
        $feedback->setEmail($email);
        $feedback->setText($text);
        $feedback->setDate(new DateTime());

        $this->_em->persist($feedback);
        $this->_em->flush();

        return $feedback->getId();
    }
}
