<?php

namespace App\Repository;

use App\Entity\ArticlesViews;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArticlesViews|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlesViews|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlesViews[]    findAll()
 * @method ArticlesViews[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesViewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArticlesViews::class);
    }

    public function getViewsByProductIds($productIds)
    {
        $qb = $this->createQueryBuilder('av')
            ->select('av.articleId, av.views')
            ->where('av.articleId IN (:productIds)')
            ->setParameter('productIds', $productIds);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param $productId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateViewByProductId($productId): void
    {
        $productView = $this->find($productId);
        if ($productView !== null) {
            $productView->setViews($productView->getViews() + 1);
        } else {
            $productView = new ArticlesViews();
            $productView->setArticleId($productId);
            $productView->setViews(1);
        }

        $this->_em->persist($productView);
        $this->_em->flush();
    }
}
