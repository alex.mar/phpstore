<?php

namespace App\Repository;

use App\Entity\AdminUsers;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AdminUsers|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminUsers|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminUsers[]    findAll()
 * @method AdminUsers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminUsersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AdminUsers::class);
    }

    /**
     * @param int $userId
     * @return string
     */
    public function getSessionKeyByUserId(int $userId)
    {
        $query = $this->createQueryBuilder('au')
            ->select('au.sessionKey')
            ->where('au.id = :userId')
            ->setParameter('userId', $userId);

        return DoctrineHelper::getStatement($query->getQuery())->fetchColumn();
    }

    /**
     * @param string $email
     * @param string $password
     * @return string
     */
    public function getIdByEmailAndPassword(string $email, string $password)
    {
        $query = $this->createQueryBuilder('au')
            ->select('au.id')
            ->where('au.email = :email')
            ->andWhere('au.password = :password')
            ->setParameters([
                'email' => $email,
                'password' => $password
            ]);

        return DoctrineHelper::getStatement($query->getQuery())->fetchColumn();
    }

    /**
     * @param int $userId
     * @param string $sessionKey
     * @return mixed
     */
    public function updateSessionKeyByUserId(int $userId, string $sessionKey)
    {
        return $this->createQueryBuilder('au')
            ->update(AdminUsers::class, 'au')
            ->set('au.sessionKey', ':sessionKey')
            ->where('au.id = :userId')
            ->setParameters([
                'sessionKey' => $sessionKey,
                'userId' => $userId
            ])
            ->getQuery()
            ->execute();
    }
}
