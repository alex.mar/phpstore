<?php

namespace App\Repository;

use App\Entity\Categories;
use App\Entity\DepartmentCategories;
use App\Entity\Departments;
use App\Entity\CategoriesMeta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Categories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categories[]    findAll()
 * @method Categories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_TITLE = 'title';
    const FIELD_H1 = 'h1';
    const FIELD_KEYWORDS = 'keywords';
    const FIELD_DESCRIPTION = 'description';

    const PROPERTY_H1_SPAN = 'h1Span';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Categories::class);
    }

    public function getAllCategories()
    {
        return $this->createQueryBuilder('c')
            ->select()
            ->getQuery()
            ->getResult(\PDO::FETCH_ASSOC);
    }

    /**
     * @param array $categoryData
     * @return int
     * @throws \Doctrine\ORM\ORMException
     */
    public function setCategory($categoryData)
    {
        $categoryId = isset($categoryData[self::FIELD_ID]) ? $categoryData[self::FIELD_ID] : 0;
        $category = $this->find($categoryId);
        if (!($category instanceof Categories)) {
            $category = new Categories();
        }
        $category->setName($categoryData[self::FIELD_NAME]);

        $this->_em->persist($category);
        $this->_em->flush();

        return $category->getId();
    }

    /**
     * @param $id
     * @param $name
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateNameById($id, $name)
    {
        $category = $this->find($id);
        $category->setName($name);

        $this->_em->flush($category);
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteById($id)
    {
        $category = $this->find($id);
        $this->_em->remove($category);

        $this->_em->flush();
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getCategoryRelations($categoryId)
    {
        return $this->createQueryBuilder('c')
            ->select('dc.id as relationId, d.id, d.name, d.url, mc.title, mc.h1, mc.h1Span,
             mc.keywords, mc.description')
            ->join(DepartmentCategories::class, 'dc', Join::WITH, 'dc.categoryId = c.id')
            ->join(Departments::class, 'd', Join::WITH, 'd.id = dc.departmentId')
            ->join(CategoriesMeta::class, 'mc', Join::WITH, 'dc.id = mc.relationId')
            ->where('c.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->getQuery()
            ->getResult();
    }
}
