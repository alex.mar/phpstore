<?php

namespace App\Repository;

use App\Entity\StaticPages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StaticPages|null find($id, $lockMode = null, $lockVersion = null)
 * @method StaticPages|null findOneBy(array $criteria, array $orderBy = null)
 * @method StaticPages[]    findAll()
 * @method StaticPages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StaticPagesRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_URL = 'url';
    const FIELD_TITLE = 'title';
    const FIELD_H1 = 'h1';
    const FIELD_KEYWORDS = 'keywords';
    const FIELD_DESCRIPTION = 'description';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StaticPages::class);
    }

    /**
     * @return array
     */
    public function getAllStaticPages()
    {
        return $this->createQueryBuilder('s')
            ->select()
            ->getQuery()
            ->getResult(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $staticPageData
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setStaticPage($staticPageData)
    {
        $staticPageId = isset($staticPageData[self::FIELD_ID]) ? $staticPageData[self::FIELD_ID] : 0;
        $staticPage = $this->find($staticPageId);
        if (!($staticPage instanceof StaticPages)) {
            $staticPage = new StaticPages();
        }

        $staticPage->setName($staticPageData[self::FIELD_NAME]);
        $staticPage->setUrl($staticPageData[self::FIELD_URL]);
        $staticPage->setH1($staticPageData[self::FIELD_H1]);
        $staticPage->setTitle($staticPageData[self::FIELD_TITLE]);
        $staticPage->setKeywords($staticPageData[self::FIELD_KEYWORDS]);
        $staticPage->setDescription($staticPageData[self::FIELD_DESCRIPTION]);

        $this->_em->persist($staticPage);
        $this->_em->flush();
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteById($id)
    {
        $staticPage = $this->find($id);
        $this->_em->remove($staticPage);

        $this->_em->flush();
    }
}
