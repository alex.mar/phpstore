<?php

namespace App\Repository;

use App\Entity\Menu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';
    const FIELD_LINK = 'link';
    const FIELD_NAME = 'name';
    const FIELD_ICON = 'icon';
    const FIELD_LEVEL = 'level';
    const FIELD_ORDER = 'order';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    /**
     * @return Menu[]
     */
    public function getAllMenuItems(): array
    {
        return $this->createQueryBuilder('m')
            ->select()
            ->orderBy('m.order')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $menuItemOrders
     * @throws DBALException
     */
    public function updateOrderByIds(array $menuItemOrders): void
    {
        $sql = 'UPDATE menu set `order` = CASE ';
        foreach ($menuItemOrders as $itemOrder) {
            $sql .= "WHEN id = {$itemOrder[self::FIELD_ID]} THEN {$itemOrder[self::FIELD_ORDER]} ";
        }
        $sql .= 'END';

        $this->_em->getConnection()->exec($sql);
    }

    /**
     * @param $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteById($id): void
    {
        $item = $this->find($id);
        $this->_em->remove($item);

        $this->_em->flush();
    }

    /**
     * @param $itemData
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setMenuItem($itemData): void
    {
        $itemId = $itemData[self::FIELD_ID] ?? 0;
        $item = $this->find($itemId);
        if (!($item instanceof Menu)) {
            $item = new Menu();
        }
        $item->setLink($itemData[self::FIELD_LINK]);
        $item->setName($itemData[self::FIELD_NAME]);
        $item->setIcon($itemData[self::FIELD_ICON]);
        $item->setLevel($itemData[self::FIELD_LEVEL]);
        $item->setOrder($itemData[Menu::FIELD_ORDER]);
        $item->setParentId($itemData[Menu::FIELD_PARENT_ID] ?? 0);
        $this->_em->persist($item);

        $this->_em->flush();
    }

    public function getMaxOrder()
    {
        $sql = 'SELECT MAX(`order`) as max FROM menu';
        return $this->_em->getConnection()->executeQuery($sql)->fetchColumn();
    }
}
