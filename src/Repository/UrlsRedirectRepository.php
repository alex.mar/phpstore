<?php

namespace App\Repository;

use App\Entity\UrlsRedirect;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UrlsRedirect|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlsRedirect|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlsRedirect[]    findAll()
 * @method UrlsRedirect[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlsRedirectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UrlsRedirect::class);
    }

    /**
     * @param $from
     * @param $to
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUrlRedirect($from, $to): void
    {
        $urlRedirect = $this->findBy([UrlsRedirect::FIELD_FROM => $from]);
        if (!($urlRedirect instanceof UrlsRedirect)) {
            $urlRedirect = new UrlsRedirect();
        }
        $urlRedirect->setFrom($from);
        $urlRedirect->setTo($to);

        $this->_em->persist($urlRedirect);
        $this->_em->flush();
    }
}
