<?php

namespace App\Repository;

use App\Entity\SitemapGeneration;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SitemapGeneration|null find($id, $lockMode = null, $lockVersion = null)
 * @method SitemapGeneration|null findOneBy(array $criteria, array $orderBy = null)
 * @method SitemapGeneration[]    findAll()
 * @method SitemapGeneration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SitemapGenerationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SitemapGeneration::class);
    }

    /**
     * @param $totalUrls
     * @param $productUrls
     * @param $categoryUrls
     * @param $departmentUrls
     * @param $staticUrls
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setSitemapGenerationStatus(
        int $totalUrls,
        int $productUrls,
        int $categoryUrls,
        int $departmentUrls,
        int $staticUrls
    ): void {
        $sitemapGeneration = $this->findAll();
        if (empty($sitemapGeneration)) {
            $sitemapGeneration = new SitemapGeneration();
        } else {
            $sitemapGeneration = array_shift($sitemapGeneration);
        }
        $sitemapGeneration->setTotal($totalUrls)
            ->setProduct($productUrls)
            ->setCategory($categoryUrls)
            ->setDepartment($departmentUrls)
            ->setStatic($staticUrls)
            ->setLastGeneration(new DateTime());

        $this->_em->persist($sitemapGeneration);
        $this->_em->flush();
    }
}
