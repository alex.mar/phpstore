<?php

namespace App\Repository;

use App\Entity\Categories;
use App\Entity\DepartmentCategories;
use App\Entity\Departments;
use App\Helper\DoctrineHelper\DoctrineHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DepartmentCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method DepartmentCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method DepartmentCategories[]    findAll()
 * @method DepartmentCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentCategoriesRepository extends ServiceEntityRepository
{
    const FIELD_ID = 'id';

    const PROPERTY_CATEGORY_ID = 'categoryId';
    const PROPERTY_DEPARTMENT_ID = 'departmentId';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DepartmentCategories::class);
    }

    /**
     * @return array
     */
    public function getAllRelations()
    {
        return $this->createQueryBuilder('dc')
            ->select('dc.categoryId, dc.departmentId, d.name as departmentName, c.name as categoryName')
            ->join(Departments::class, 'd', Join::WITH, 'dc.departmentId = d.id')
            ->join(Categories::class, 'c', Join::WITH, 'dc.categoryId = c.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $departmentId
     * @return array
     */
    public function getRelationsByDepartmentId($departmentId)
    {
        return $this->createQueryBuilder('dc')
            ->select('dc.categoryId, dc.departmentId, d.name as departmentName, c.name as categoryName')
            ->join(Departments::class, 'd', Join::WITH, 'dc.departmentId = d.id')
            ->join(Categories::class, 'c', Join::WITH, 'dc.categoryId = c.id')
            ->where('dc.departmentId = :departmentId')
            ->setParameter('departmentId', $departmentId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $categoryId
     * @param array $departmentIds
     * @throws \Doctrine\DBAL\DBALException
     */
    public function addRelationsByCategory($categoryId, $departmentIds)
    {
        $sql = 'INSERT IGNORE INTO department_categories (`id`, category_id, department_id) VALUES ';
        foreach ($departmentIds as $departmentId) {
            $sql .= "(NULL, {$categoryId}, {$departmentId}),";
        }
        $sql = substr($sql, 0, -1);
        $this->_em->getConnection()->exec($sql);
    }

    /**
     * @param $categoryId
     * @param $departmentIds
     * @throws \Doctrine\DBAL\DBALException
     */
    public function removeRelationsByCategory($categoryId, $departmentIds)
    {
        $query = $this->getEntityManager()->createQuery('DELETE FROM App\Entity\DepartmentCategories dc
                WHERE dc.categoryId = :categoryId AND dc.departmentId IN (:departmentIds)')
            ->setParameter('categoryId', $categoryId)
            ->setParameter('departmentIds', $departmentIds);
        $query->execute();
    }

    /**
     * @param int $categoryId
     * @param array $departmentIds
     * @return DepartmentCategories[]
     */
    public function getRelationsByCategoryIdAndDepartmentIds($categoryId, $departmentIds)
    {
        return $this->createQueryBuilder('dc')
            ->select()
            ->where('dc.categoryId = :categoryId')
            ->andWhere('dc.departmentId IN (:departmentIds)')
            ->setParameters([
                'categoryId' => $categoryId,
                'departmentIds' => $departmentIds
            ])
            ->getQuery()
            ->getResult();
    }

    public function getIdByCategoryIdAndDepartmentId($categoryId, $departmentId)
    {
        $qb = $this->createQueryBuilder('dc')
            ->select('dc.id')
            ->where('dc.categoryId = :categoryId')
            ->andWhere('dc.departmentId = :departmentId')
            ->setParameters([
                'categoryId' => $categoryId,
                'departmentId' => $departmentId
            ]);

        return (int) DoctrineHelper::getStatement($qb->getQuery())->fetchColumn();
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteRelationById($id)
    {
        $relation = $this->find($id);
        if ($relation instanceof DepartmentCategories) {
            $this->_em->remove($relation);
            $this->_em->flush();
        }
    }
}
