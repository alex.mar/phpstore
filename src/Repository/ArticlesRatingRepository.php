<?php

namespace App\Repository;

use App\Entity\ArticlesRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArticlesRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlesRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlesRating[]    findAll()
 * @method ArticlesRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesRatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArticlesRating::class);
    }

    /**
     * @param $articleId
     * @return ArticlesRating
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getRatingByArticleId($articleId): ArticlesRating
    {
        try {
            $articleRating = $this->createQueryBuilder('ar')
                ->select()
                ->where('ar.articleId = :articleId')
                ->setParameter('articleId', $articleId)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            $articleRating = new ArticlesRating();
            $articleRating->setArticleId($articleId);
            $articleRating->setRatingUp(0);
            $articleRating->setRatingDown(0);
            $this->_em->persist($articleRating);
            $this->_em->flush();
        }

        return $articleRating;
    }

    /**
     * @param $articleId
     * @param $ratingUp
     * @param $ratingDown
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setArticleRating($articleId, $ratingUp, $ratingDown): void
    {
        $articleRating = $this->findOneBy(compact('articleId'));
        if (!($articleRating instanceof ArticlesRating)) {
            $articleRating = new ArticlesRating();
        }
        $articleRating->setArticleId($articleId);
        $articleRating->setRatingUp($ratingUp);
        $articleRating->setRatingDown($ratingDown);

        $this->_em->persist($articleRating);
        $this->_em->flush();
    }
}
