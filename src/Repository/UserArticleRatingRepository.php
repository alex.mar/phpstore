<?php

namespace App\Repository;

use App\Entity\UserArticleRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserArticleRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserArticleRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserArticleRating[]    findAll()
 * @method UserArticleRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserArticleRatingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserArticleRating::class);
    }

    public function getVoteByArticleIdAndIp($articleId, $ip): int
    {
        $userArticleRating = $this->findOneBy(compact('articleId', 'ip'));
        if ($userArticleRating instanceof UserArticleRating) {
            return $userArticleRating->getVote();
        }

        return 0;
    }

    /**
     * @param $ip
     * @param $articleId
     * @param $vote
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setUserVote($ip, $articleId, $vote): void
    {
        $userArticleRating = $this->findOneBy(compact('ip', 'articleId'));
        if (!($userArticleRating instanceof UserArticleRating)) {
            $userArticleRating = new UserArticleRating();
        }
        $userArticleRating->setArticleId($articleId);
        $userArticleRating->setIp($ip);
        $userArticleRating->setVote($vote);

        $this->_em->persist($userArticleRating);
        $this->_em->flush();
    }

    /**
     * @param $ip
     * @param $articleId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete($ip, $articleId): void
    {
        $userArticleRating = $this->findOneBy(compact('ip', 'articleId'));
        if ($userArticleRating instanceof UserArticleRating) {
            $this->_em->remove($userArticleRating);
            $this->_em->flush();
        }
    }
}
