<?php

namespace App\Repository;

use App\Entity\ArticlesImages;
use App\Helper\DoctrineHelper\DoctrineHelper;
use App\Service\ProductLoader\Loaders\ArticleProductsLoader;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArticlesImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlesImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlesImages[]    findAll()
 * @method ArticlesImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesImagesRepository extends ServiceEntityRepository
{
    const FIELD_PATH = 'path';
    const FIELD_ALT = 'alt';
    const FIELD_POSITION = 'position';

    const PROPERTY_ARTICLE_ID = 'articleId';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArticlesImages::class);
    }

    /**
     * @param array $articleData
     * @throws \Doctrine\DBAL\DBALException
     */
    public function setArticleImages($articleData)
    {
        $articleImages = $articleData[ArticleProductsLoader::KEY_IMAGES];
        $query = "INSERT INTO `articles_images` (`article_id`, `path`, `alt`, `position`) VALUES ";

        foreach ($articleImages as $articleImage) {
            $query .= "({$articleData[ArticlesRepository::FIELD_ID]}, '{$articleImage[self::FIELD_PATH]}',
             '{$articleImage[self::FIELD_ALT]}', {$articleImage[self::FIELD_POSITION]}),";
        }
        $query = substr($query, 0, -1);
        $query .= ' ON DUPLICATE KEY UPDATE `path` = VALUES(path), `alt` = VALUES(alt)';

        $this->_em->getConnection()->executeQuery($query);
    }

    public function getImagesByArticleId($articleId)
    {
        $qb = $this->createQueryBuilder('am')
            ->select()
            ->where('am.articleId = :articleId')
            ->setParameter('articleId', $articleId);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getImagesByArticleIds($articleIds)
    {
        $qb = $this->createQueryBuilder('am')
            ->select()
            ->where('am.articleId IN (:articleIds)')
            ->setParameter('articleIds', $articleIds);

        return DoctrineHelper::getStatement($qb->getQuery())->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $imageId
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteImageById($imageId)
    {
        $image = $this->find($imageId);
        $this->_em->remove($image);

        $this->_em->flush();
    }
}
