<?php

namespace App\Controller\PhpStore;

use App\Entity\Articles;
use App\Entity\Departments;
use App\Repository\ArticlesRepository;
use App\Repository\SearchStatisticRepository;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\ProductLoader\ProductLoaderService;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;

class SearchPageController extends BaseController
{
    /**
     * @param ArticlesRepository $articlesRepository
     * @param ProductLoaderFactory $productLoaderFactory
     * @param SearchStatisticRepository $searchStatisticRepository
     * @return Response
     * @throws DBALException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function search(
        ArticlesRepository $articlesRepository,
        ProductLoaderFactory $productLoaderFactory,
        SearchStatisticRepository $searchStatisticRepository
    ): Response {
        $query = $this->request->get('q');
        $query = trim(strip_tags($query));
        $result = [];
        if (empty($query)) {
            throw $this->createNotFoundException();
        }

        $words = explode(' ', $query);
        $articles = $articlesRepository->search($words);
        if (!empty($articles)) {
            $articleIds = array_column($articles, Articles::FIELD_ID);
            $productLoader = $productLoaderFactory->buildByDepartmentId(Departments::DEPARTMENT_ID_ARTICLE);
            $articles = $productLoader->getProductsByIds($articleIds);
            foreach ($articles as $index => $article) {
                $h1 = $article['h1'];
                $text = $article['text'];
                $text = ProductLoaderService::getDecodeText($text);
                $text = ProductLoaderService::getShortText($text);
                $text = preg_replace('/<em>/i', '', $text);
                $text = preg_replace('/<\/em>/i', '', $text);
                $text = preg_replace('/<span style="font-weight: bolder;">/i', '', $text);
                $text = preg_replace('/<\/span>/i', '', $text);
                $text = preg_replace('/<i>/i', '', $text);
                $text = preg_replace('/<\/i>/i', '', $text);
                foreach ($words as $word) {
                    $text = preg_replace("/$word/i", "<b>$word</b>", $text);
                    $h1 = preg_replace("/$word/i", "<b>$word</b>", $h1);
                }
                $articles[$index]['text'] = $text;
                $articles[$index]['h1'] = $h1;
            }
        }
        $result['articles'] = $articles;

        $resultArticles = count($articles);
        $result['count'] = $resultArticles;

        $date = date('Y-m-d');
        $searchStatisticRepository->setSearchQuery($query, $date, $resultArticles);

        return $this->render('phpstore/search-page/search-page.html.twig', [
            'query' => $query,
            'result' => $result
        ]);
    }
}
