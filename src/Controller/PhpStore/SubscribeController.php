<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 01.01.2019
 * Time: 22:58
 */

namespace App\Controller\PhpStore;

use App\Repository\UsersSubscribeRepository;
use App\Service\Department\DepartmentService;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\StaticPage\StaticPageService;
use App\Service\Url\UrlService;
use App\Service\Validator\ValidatorService;
use App\UrlData\UrlDataFactory;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class SubscribeController extends BaseController
{
    /**
     * @param DepartmentService $departmentService
     * @param DepartmentCategoriesService $departmentCategoriesService
     * @param UsersSubscribeRepository $usersSubscribeRepository
     * @param StaticPageService $staticPageService
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function subscribe(
        DepartmentService $departmentService,
        DepartmentCategoriesService $departmentCategoriesService,
        UsersSubscribeRepository $usersSubscribeRepository,
        StaticPageService $staticPageService
    ): Response {
        $submit = $this->request->get('submit');
        if ($submit) {
            $name = ValidatorService::validateTags($this->request->get('name'));
            $email = ValidatorService::validateTags($this->request->get('email'));
            $departmentId = (int) $this->request->get('department');
            $categoryId = (int) $this->request->get('category');
            $subscribeId = $usersSubscribeRepository->getUserSubscribeId($name, $email, $departmentId, $categoryId);
            if (empty($subscribeId)) {
                $subscribeId = $usersSubscribeRepository->setUserSubscribe($name, $email, $departmentId, $categoryId);
                $session = new Session();
                $session->set('subscribeId', $subscribeId);

                return $this->redirectToRoute('app_subscribe_success');
            }

            return $this->redirectToRoute('app_subscribe_error', ['status' => 'existSubscribe']);
        }
        $action = $this->request->get('action');
        if ($action) {
            $response = [];
            switch ($action) {
                case 'loadCategories':
                    $departmentId = $this->request->get('departmentId');
                    $relations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId($departmentId);
                    $categories = $relations[DepartmentCategoriesService::KEY_CATEGORIES];
                    $response['categories'] = $categories;
                    break;
            }

            return $this->json($response);
        }

        $meta = $staticPageService->getStaticPageEntryById($this->urlData->getEntityId());

        $departments = $departmentService->getAllDepartments();
        return $this->render('phpstore/subscribe/subscribe.html.twig', [
            'departments' => $departments,
            'meta' => $meta
        ]);
    }

    /**
     * @param StaticPageService $staticPageService
     * @param UrlDataFactory $urlDataFactory
     * @param UsersSubscribeRepository $usersSubscribeRepository
     * @return Response
     */
    public function subscribeSuccess(
        StaticPageService $staticPageService,
        UrlDataFactory $urlDataFactory,
        UsersSubscribeRepository $usersSubscribeRepository
    ): Response {
        $session = new Session();

        $subscribeId = $session->get('subscribeId');
        if (empty($subscribeId)) {
            return $this->redirectToRoute('app_subscribe');
        }

        $subscribe = $usersSubscribeRepository->find($subscribeId);
        if ($subscribe === null) {
            return $this->redirectToRoute('app_subscribe_error', ['status' => 'wrongSubscribeId']);
        }

        $userName = $subscribe->getName();

        $url = $this->generateUrl('app_subscribe');
        $urlData = $urlDataFactory->build(UrlService::formatTableUri($url));
        $meta = $staticPageService->getStaticPageEntryById($urlData->getEntityId());

        $session->remove('subscribeId');

        return $this->render('phpstore/subscribe/subscribe-success.html.twig', [
            'meta' => $meta,
            'userName' => $userName
        ]);
    }

    public function subscribeError(
        StaticPageService $staticPageService,
        UrlDataFactory $urlDataFactory
    ) {
        $status = $this->request->get('status');

        if (empty($status)) {
            return $this->redirectToRoute('app_subscribe');
        }

        $text = 'Произошла ошибка!<br> Попробуйте оформить подписку позже';
        switch ($status) {
            case 'existSubscribe':
                $text = 'Вы уже подписаны на уведомления!
                         <br> Вы можете оформить подписку на другие категории или разделы на сайте';
                break;
            case 'wrongSubscribeId':
                $text = 'Ошибка аутентификации!
                         <br> Попробуйте оформить подписку позже';
                break;
            default:
                break;
        }

        $url = $this->generateUrl('app_subscribe');
        $urlData = $urlDataFactory->build(UrlService::formatTableUri($url));
        $meta = $staticPageService->getStaticPageEntryById($urlData->getEntityId());

        return $this->render('phpstore/subscribe/subscribe-error.html.twig', [
            'meta' => $meta,
            'text' => $text
        ]);
    }
}
