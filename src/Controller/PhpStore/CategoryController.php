<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 24.03.2019
 * Time: 21:21
 */

namespace App\Controller\PhpStore;

use App\Repository\CategoriesMetaRepository;
use App\Repository\DepartmentCategoriesRepository;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\ProductLoader\ProductLoaderService;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends BaseController
{
    /**
     * @param ProductLoaderFactory $productLoaderFactory
     * @param DepartmentCategoriesRepository $departmentCategoriesRepository
     * @param CategoriesMetaRepository $categoriesMetaRepository
     * @return Response
     * @throws NonUniqueResultException
     */
    public function category(
        ProductLoaderFactory $productLoaderFactory,
        DepartmentCategoriesRepository $departmentCategoriesRepository,
        CategoriesMetaRepository $categoriesMetaRepository
    ): Response {
        $relationId = $departmentCategoriesRepository->getIdByCategoryIdAndDepartmentId(
            $this->urlData->getCategoryId(),
            $this->urlData->getDepartmentId()
        );
        $meta = $categoriesMetaRepository->getMetaByRelationId($relationId);

        $sort = $this->request->get('sort');
        if ($sort === null) {
            $sort = 'date';
        }
        $productLoader = $productLoaderFactory->buildByDepartmentId($this->urlData->getDepartmentId());
        $products = $productLoader->getProductsByCategoryIds([$this->urlData->getCategoryId()], $sort);
        $count = count($products);
        foreach ($products as $index => $article) {
            $text = $article['text'];
            $text = ProductLoaderService::getDecodeText($text);
            $text = ProductLoaderService::getShortText($text);
            $products[$index]['text'] = $text;
        }

        $productsBlock = [
            'count' => $count,
            'products' => $products,
            'sort' => $sort,
            'pagination' => []
        ];

        return $this->render($this->getBaseTemplatePath() . 'category/category.html.twig', [
            'meta' => $meta,
            'productsBlock' => $productsBlock
        ]);
    }
}