<?php

namespace App\Controller\PhpStore;

use App\Repository\FeedbackRepository;
use App\Service\Mailer\Configuration\ConfigurationBuilder;
use App\Service\Mailer\MailerService;
use App\Service\StaticPage\StaticPageService;
use App\Service\Url\UrlService;
use App\Service\Validator\ValidatorService;
use App\UrlData\UrlDataFactory;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Swift_Message;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class FeedbackController extends BaseController
{
    /**
     * @param StaticPageService $staticPageService
     * @param FeedbackRepository $feedbackRepository
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function feedback(
        StaticPageService $staticPageService,
        FeedbackRepository $feedbackRepository
    ): Response {
        $submit = $this->request->get('submit');
        if ($submit) {
            $name = ValidatorService::validateTags($this->request->get('name'));
            $email = ValidatorService::validateTags($this->request->get('email'));
            $text = ValidatorService::validateTags($this->request->get('text'));

            $mailerConfigurationBuilder = new ConfigurationBuilder();
            $configuration = $mailerConfigurationBuilder->buildNoReply($this->params);

            $mailerService = new MailerService($configuration);

            $feedbackId = $feedbackRepository->setFeedback($name, $email, $text);

            $feedback = [
                'name' => $name,
                'email' => $email,
                'text' => $text
            ];

            $mailerAdmins = (array) $this->params->get('mailer_admins');
            foreach ($mailerAdmins as $adminMail => $adminPassword) {
                $message = (new Swift_Message('Feedback'))
                    ->setFrom([$configuration->getUser() => 'PhpStore'])
                    ->setTo($adminMail)
                    ->setBody($this->renderView('phpstore/emails/admin-feedback.html.twig', [
                        'feedback' => $feedback
                    ]), 'text/html');

                $mailerService->send($message);
            }

            $session = new Session();
            $session->set('feedbackId', $feedbackId);

            return $this->redirectToRoute('app_feedback_success');
        }

        $meta = $staticPageService->getStaticPageEntryById($this->urlData->getEntityId());

        return $this->render('phpstore/feedback/feedback.html.twig', [
            'meta' => $meta
        ]);
    }

    /**
     * @param StaticPageService $staticPageService
     * @param FeedbackRepository $feedbackRepository
     * @param UrlDataFactory $urlDataFactory
     * @return RedirectResponse|Response
     */
    public function feedbackSuccess(
        StaticPageService $staticPageService,
        FeedbackRepository $feedbackRepository,
        UrlDataFactory $urlDataFactory
    ): Response {
        $session = new Session();

        $feedbackId = $session->get('feedbackId');
        if (empty($feedbackId)) {
            return $this->redirectToRoute('app_feedback');
        }

        $feedback = $feedbackRepository->find($feedbackId);
        if ($feedback === null) {
            return $this->redirectToRoute('app_feedback');
        }

        $userName = $feedback->getName();

        $url = $this->generateUrl('app_feedback');
        $urlData = $urlDataFactory->build(UrlService::formatTableUri($url));
        $meta = $staticPageService->getStaticPageEntryById($urlData->getEntityId());

        return $this->render('phpstore/feedback/feedback-success.html.twig', [
            'meta' => $meta,
            'userName' => $userName
        ]);
    }
}
