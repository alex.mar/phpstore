<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 24.03.2019
 * Time: 9:48
 */

namespace App\Controller\PhpStore;

use App\Blocks\CategoriesBlock;
use App\Service\Department\DepartmentService;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\ProductLoader\ProductLoaderFactory;
use App\Service\ProductLoader\ProductLoaderService;

class DepartmentController extends BaseController
{
    public function department(
        DepartmentCategoriesService $departmentCategoriesService,
        ProductLoaderFactory $productLoaderFactory,
        CategoriesBlock $categoriesBlock,
        DepartmentService $departmentService
    ) {
        $categoriesBlock = $categoriesBlock->loadContent($this->urlData);
        $departmentId = $this->urlData->getDepartmentId();

        $meta = $departmentService->getDepartmentEntryById($departmentId);
        $categories = $departmentCategoriesService->getDepartmentRelationsByDepartmentId($departmentId);
        $categoryIds = array_keys($categories[DepartmentCategoriesService::KEY_CATEGORIES]);

        $sort = $this->request->get('sort');
        if ($sort === null) {
            $sort = 'date';
        }
        $productLoader = $productLoaderFactory->buildByDepartmentId($this->urlData->getDepartmentId());
        $products = $productLoader->getProductsByCategoryIds($categoryIds, $sort);
        $count = count($products);
        foreach ($products as $index => $article) {
            $text = $article['text'];
            $text = ProductLoaderService::getDecodeText($text);
            $text = ProductLoaderService::getShortText($text);
            $products[$index]['text'] = $text;
        }

        $productsBlock = [
            'count' => $count,
            'products' => $products,
            'sort' => $sort,
            'pagination' => []
        ];

        return $this->render($this->getBaseTemplatePath() . 'department/department.html.twig', [
            'meta' => $meta,
            'categories' => $categoriesBlock,
            'productsBlock' => $productsBlock
        ]);
    }
}
