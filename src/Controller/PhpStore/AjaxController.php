<?php

namespace App\Controller\PhpStore;

use App\Repository\ArticlesRatingRepository;
use App\Repository\UserArticleRatingRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends AbstractController
{
    /**
     * @param Request $request
     * @param UserArticleRatingRepository $userArticleRatingRepository
     * @param ArticlesRatingRepository $articlesRatingRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateArticleRating(
        Request $request,
        UserArticleRatingRepository $userArticleRatingRepository,
        ArticlesRatingRepository $articlesRatingRepository
    ): JsonResponse {
        $articleId = (int) $request->get('articleId');
        $ratingUp = (int) $request->get('ratingUp');
        $ratingDown = (int) $request->get('ratingDown');
        $vote = (int) $request->get('vote');
        $ip = $request->getClientIp();
        $userArticleRatingRepository->setUserVote($ip, $articleId, $vote);
        $articlesRatingRepository->setArticleRating($articleId, $ratingUp, $ratingDown);

        return new JsonResponse([]);
    }
}
