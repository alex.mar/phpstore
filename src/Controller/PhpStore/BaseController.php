<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 28.12.2018
 * Time: 17:03
 */

namespace App\Controller\PhpStore;

use App\Service\BreadCrumbs\BreadCrumbsService;
use App\Service\Menu\MenuService;
use App\Service\UrlRedirect\UrlRedirectService;
use App\UrlData\UrlData;
use App\UrlData\UrlDataFactoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    /** @var \Twig_Environment */
    protected $template;

    /** @var Request|null  */
    protected $request;

    /** @var ParameterBagInterface */
    protected $params;

    /** @var UrlData */
    protected $urlData;

    /**
     * BaseController constructor.
     * @param RequestStack $request
     * @param ParameterBagInterface $params
     * @param MenuService $menuService
     * @param UrlDataFactoryInterface $urlDataFactory
     * @param BreadCrumbsService $breadCrumbsService
     * @param UrlRedirectService $urlRedirectService
     * @param \Twig_Environment $template
     */
    public function __construct(
        RequestStack $request,
        ParameterBagInterface $params,
        MenuService $menuService,
        UrlDataFactoryInterface $urlDataFactory,
        BreadCrumbsService $breadCrumbsService,
        UrlRedirectService $urlRedirectService,
        \Twig_Environment $template
    ) {
        $this->request = $request->getCurrentRequest();
        $this->template = $template;
        $this->params = $params;

        $currentRequest = $request->getCurrentRequest();
        if ($currentRequest === null) {
            throw $this->createNotFoundException('Null pointer request');
        }
        $test = [];

        $redirectUrl = $urlRedirectService->getRedirectUrl(strtok($currentRequest->getRequestUri(), '?'));
        if (!empty($redirectUrl)) {
            $this->redirect($redirectUrl, Response::HTTP_MOVED_PERMANENTLY)->send();
        }

        $this->urlData = $urlDataFactory->build(strtok($currentRequest->getRequestUri(), '?'));

        $menu = $menuService->loadMenu($this->urlData);
        $breadCrumbs = $breadCrumbsService->buildBreadCrumbsByUrlData($this->urlData);

        $this->template->addGlobal('menu', $menu);
        $this->template->addGlobal('breadcrumbs', $breadCrumbs);
    }

    /**
     * @return string
     */
    protected function getBaseTemplatePath(): string
    {
        return 'phpstore/';
    }
}