<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 06.01.2019
 * Time: 15:50
 */

namespace App\Controller\Admin;

use App\Service\Authorization\AdminAuthorizationService;
use App\Service\Menu\AdminMenuService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class BaseController extends AbstractController
{
    const ACTION_SAVE = 'save';
    const ACTION_RUN = 'run';

    const KEY_MESSAGE = 'message';
    const KEY_STATUS = 'status';

    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /** @var \Twig_Environment */
    protected $template;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(
        RequestStack $request,
        \Twig_Environment $template,
        AdminMenuService $menuService,
        AdminAuthorizationService $authorizationService
    ) {
        $this->request = $request->getCurrentRequest();
        $this->template = $template;

        $authorizationService->checkAuth();
        $menu = $menuService->loadMenu();

        $this->template->addGlobal('menu', $menu);
    }
}