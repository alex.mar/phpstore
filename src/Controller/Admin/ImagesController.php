<?php

namespace App\Controller\Admin;

use App\Form\UploaderImages\ImagesFolder;
use App\Form\UploaderImages\ImagesType;
use App\Service\FileUploader\FileUploader;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImagesController extends BaseController
{
    const DIR_PUBLIC = 'public';
    const DIR_IMG = 'images';

    public function images()
    {
        $imagesFolder = new ImagesFolder();
        $form = $this->createForm(ImagesType::class, $imagesFolder);
        $form->handleRequest($this->request);

        $actionResponse = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $path = $imagesFolder->getPath();
            $images = $imagesFolder->getImages();
            try {
                $fileUploader = new FileUploader($path);
                $fileUploader->correctImgPath();
                $fileUploader->uploadFiles($images);
                $actionResponse = 'success';
            } catch (FileException $e) {
                $actionResponse = 'error';
            }
        }

        return $this->render('admin/images/images.html.twig', [
            'form' => $form->createView(),
            'actionResponse' => $actionResponse
        ]);
    }
}
