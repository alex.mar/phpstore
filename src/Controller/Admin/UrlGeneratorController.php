<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 16.03.2019
 * Time: 23:32
 */

namespace App\Controller\Admin;

use App\Service\UrlGenerator\UrlGenerator;
use App\Service\UrlType\UrlTypeService;

class UrlGeneratorController extends BaseController
{
    public function urlGenerator(
        UrlTypeService $urlTypeService,
        UrlGenerator $urlGenerator
    ) {
        $urlTypes = $urlTypeService->getAllUrlTypes();
        $action = $this->request->get('action');
        switch ($action) {
            case self::ACTION_RUN:
                $selectedUrlTypes = $this->request->get('selectedUrlTypes');
                $urlGenerator->run($selectedUrlTypes);
                break;
        }
        return $this->render('admin/url-generator/url-generator.html.twig', [
            'urlTypes' => $urlTypes
        ]);
    }
}