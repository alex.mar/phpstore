<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 15.03.2019
 * Time: 23:47
 */

namespace App\Controller\Admin;

use App\Service\UrlType\UrlTypeService;
use Symfony\Component\HttpFoundation\JsonResponse;

class UrlTypesController extends BaseController
{
    public function urlTypes(
        UrlTypeService $urlTypeService
    ) {
        $urlTypes = $urlTypeService->getAllUrlTypes();
        return $this->render('admin/url-types/url-types.html.twig', [
            'urlTypes' => $urlTypes
        ]);
    }

    /**
     * @param UrlTypeService $urlTypeService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(
        UrlTypeService $urlTypeService
    ) {
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $urlType = $this->request->get('urlType');
                    $urlTypeService->setUrlType($urlType);
                    $message = 'Url Type успешно создан!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }
        return $this->render('admin/url-types/create.html.twig');
    }

    /**
     * @param $id
     * @param UrlTypeService $urlTypeService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit($id, UrlTypeService $urlTypeService)
    {
        $urlTypeEntry = $urlTypeService->getUrlTypeEntryById($id);
        if (empty($urlTypeEntry)) {
            throw $this->createNotFoundException('Url Type not found');
        }

        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action. Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $urlType = $this->request->get('urlType');
                    $urlTypeService->setUrlType($urlType);
                    $message = 'Изменения успешно сохранены!';
                    break;
                default:
                    break;
            }
            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        return $this->render('admin/url-types/edit.html.twig', ['urlType' => $urlTypeEntry]);
    }

    /**
     * @param $id
     * @param UrlTypeService $urlTypeService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($id, UrlTypeService $urlTypeService)
    {
        $urlType = $urlTypeService->getUrlTypeEntryById($id);
        if (empty($urlType)) {
            throw $this->createNotFoundException('Url Type not found');
        }

        $urlTypeService->deleteUrlTypeById($id);
        $message = 'Url Type успешно удален!';

        return new JsonResponse([self::KEY_MESSAGE => $message]);
    }
}