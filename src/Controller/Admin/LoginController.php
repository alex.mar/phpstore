<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 06.01.2019
 * Time: 21:00
 */

namespace App\Controller\Admin;

use App\Service\Authorization\AdminAuthorizationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LoginController extends AbstractController
{
    /**
     * @param Request $request
     * @param SessionInterface $session
     * @param AdminAuthorizationService $authorizationService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(
        Request $request,
        SessionInterface $session,
        AdminAuthorizationService $authorizationService
    ) {
        $submit = $request->get('submit');
        if (!empty($submit)) {
            $email = $request->get('email');
            $password = $request->get('password');

            $userId = $authorizationService->checkIssetUser($email, $password);
            if (!$userId) {
                $this->addFlash('notice', 'Неверный логин или пароль!!!');
                return $this->redirectToRoute('admin_login');
            }

            $sessionKey = md5(uniqid(rand(), true));

            $authorizationService->setSessionKey($userId, $sessionKey);

            $session->set(AdminAuthorizationService::SESSION_USER_KEY, $sessionKey);
            $session->set(AdminAuthorizationService::SESSION_USER_ID, $userId);

            return $this->redirectToRoute('admin_home');
        }
        return $this->render('admin/login/login.html.twig');
    }
}