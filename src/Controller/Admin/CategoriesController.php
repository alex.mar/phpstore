<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 08.01.2019
 * Time: 21:28
 */

namespace App\Controller\Admin;

use App\Repository\CategoriesRepository;
use App\Service\Blocks\BlocksService;
use App\Service\Category\CategoriesService;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\Department\DepartmentService;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoriesController extends BaseController
{
    const ACTION_SAVE = 'save';
    const ACTION_DELETE_RELATION = 'deleteRelation';

    const SECTION_GENERAL = 'general';
    const SECTION_RELATIONS = 'relations';

    const KEY_MESSAGE = 'message';
    const KEY_DEPARTMENTS = 'departments';

    public function categories(
        DepartmentCategoriesService $departmentCategoriesService
    ) {
        $categories = $departmentCategoriesService->getCategoryDepartmentRelations();

        return $this->render('admin/categories/categories.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @param int $id
     * @param CategoriesService $categoriesService
     * @param DepartmentService $departmentService
     * @param DepartmentCategoriesService $departmentCategoriesService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function edit(
        $id,
        CategoriesService $categoriesService,
        DepartmentService $departmentService,
        DepartmentCategoriesService $departmentCategoriesService
    ) {
        $category = $categoriesService->getCategoryEntryById($id);
        if (empty($category)) {
            throw $this->createNotFoundException('Category not found');
        }

        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте позже!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $category = $this->request->get('category');
                    $categoriesService->setCategory($category);
                    $message = 'Изменения успешно сохранены!';
                    break;
                case self::ACTION_DELETE_RELATION:
                    $relationId = $this->request->get('relationId');
                    $departmentCategoriesService->deleteRelationById($relationId);
                    $message = 'Связь успешно удалена!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }
        $departments = $departmentService->getAllDepartments();

        $categoryMetaData = $categoriesService->getCategoryRelations($id);
        $category = $category + $categoryMetaData[$id];

        return $this->render('admin/categories/edit.html.twig', [
            'category' => $category,
            'departments' => $departments
        ]);
    }

    /**
     * @param int $id
     * @param CategoriesService $categoriesService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(
        $id,
        CategoriesService $categoriesService
    ) {
        $category = $categoriesService->getCategoryEntryById($id);
        if (empty($category)) {
            throw $this->createNotFoundException('Category not found');
        }
        $categoriesService->deleteCategoryById($id);
        $message = 'Категория успешно удалена!';

        return new JsonResponse([self::KEY_MESSAGE => $message]);
    }

    /**
     * @param DepartmentService $departmentService
     * @param CategoriesService $categoriesService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(
        DepartmentService $departmentService,
        CategoriesService $categoriesService
    ) {
        $action = $this->request->get('action');
        switch ($action) {
            case self::ACTION_SAVE:
                $category = $this->request->get('category');
                $categoryName = $category[CategoriesRepository::FIELD_NAME];

                $categoryId = $categoriesService->getCategoryIdByName($categoryName);

                if (empty($categoryId)) {
                    $categoriesService->setCategory($category);
                    $message = 'Категория успешно создана!';
                } else {
                    $message = 'Категория уже существует!';
                }

                return new JsonResponse(['message' => $message]);
        }
        $departments = $departmentService->getAllDepartments();
        return $this->render('admin/categories/create.html.twig', [
            'departments' => $departments
        ]);
    }
}