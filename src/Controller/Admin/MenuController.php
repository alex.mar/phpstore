<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 04.03.2019
 * Time: 21:31
 */

namespace App\Controller\Admin;

use App\Entity\Menu;
use App\Entity\Urls;
use App\Entity\UrlType;
use App\Repository\UrlsRepository;
use App\Repository\DepartmentsRepository;
use App\Repository\MenuRepository;
use App\Repository\StaticPagesRepository;
use App\Service\Category\CategoriesService;
use App\Service\Department\DepartmentService;
use App\Service\Menu\MenuService;
use App\Service\StaticPage\StaticPageService;
use App\Service\Url\UrlService;
use App\Service\UrlType\UrlTypeService;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends BaseController
{
    const KEY_URL = 'url';
    const ACTION_SAVE = 'save';

    /**
     * @param MenuService $menuService
     * @param UrlService $urlService
     * @return JsonResponse|Response
     * @throws DBALException
     */
    public function menu(
        MenuService $menuService,
        UrlService $urlService
    ) {
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $menuOrder = $this->request->get('menuOrder');
                    $menuService->updateOrderByIds($menuOrder);
                    $message = 'Сортировка успешно сохранена!';
                    break;
                default:
                    break;
            }
            return new JsonResponse(['message' => $message]);
        }

        $menu = $menuService->getAllMenuItems();
        foreach ($menu as $key => $item) {
            $menu[$key][self::KEY_URL]
                = $urlService->getDomainUrl() . '/' . ltrim($item[MenuRepository::FIELD_LINK], '/');
        }
        return $this->render('admin/menu/menu.html.twig', [
            'frontMenu' => $menu
        ]);
    }

    /**
     * @param DepartmentService $departmentService
     * @param StaticPageService $staticPageService
     * @param UrlTypeService $urlTypeService
     * @param MenuService $menuService
     * @param UrlsRepository $catalogUrlsRepository
     * @param CategoriesService $categoriesService
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(
        DepartmentService $departmentService,
        StaticPageService $staticPageService,
        UrlTypeService $urlTypeService,
        MenuService $menuService,
        UrlsRepository $catalogUrlsRepository,
        CategoriesService $categoriesService,
        MenuRepository $menuRepository
    ) {
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $menu = $this->request->get('menuItem');
                    $maxOrder = $menuRepository->getMaxOrder();
                    $menu[Menu::FIELD_ORDER] = $maxOrder + 10;
                    $menuService->setMenuItem($menu);
                    $message = 'Элемент успешно добавлен в главное меню!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        $departments = $departmentService->getAllDepartments();
        $departmentUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_DEPARTMENT);
        $departmentUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $departmentUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($departments)
        ]);
        $departmentIdToUrl = [];
        foreach ($departmentUrls as $departmentUrl) {
            $departmentIdToUrl[$departmentUrl->getEntityId()] = $departmentUrl;
        }
        foreach ($departments as $departmentId => $department) {
            if (!isset($departmentIdToUrl[$departmentId])) {
                unset($departments[$departmentId]);
                continue;
            }
            /** @var Urls $catalogUrl */
            $catalogUrl = $departmentIdToUrl[$departmentId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($departments[$departmentId]);
                continue;
            }
            $departments[$departmentId][DepartmentsRepository::FIELD_URL] = $catalogUrl->getUrl();
        }

        $categories = $categoriesService->getAllCategories();
        $categoryUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_CATEGORY);
        $categoryUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $categoryUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($categories)
        ]);
        $categoryIdToUrl = [];
        foreach ($categoryUrls as $categoryUrl) {
            $categoryIdToUrl[$categoryUrl->getEntityId()] = $categoryUrl;
        }
        foreach ($categories as $categoryId => $category) {
            if (!isset($categoryIdToUrl[$categoryId])) {
                unset($categories[$categoryId]);
                continue;
            }
            $catalogUrl = $categoryIdToUrl[$categoryId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($categories[$categoryId]);
                continue;
            }
            $categories[$categoryId][Urls::FIELD_URL] = $catalogUrl->getUrl();
        }

        $staticPages = $staticPageService->getAllStaticPages();
        $staticPageUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_STATIC_PAGE);
        $staticPageUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $staticPageUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($staticPages)
        ]);
        $staticPageIdToUrl = [];
        foreach ($staticPageUrls as $staticPageUrl) {
            $staticPageIdToUrl[$staticPageUrl->getEntityId()] = $staticPageUrl;
        }
        foreach ($staticPages as $staticPageId => $staticPage) {
            if (!isset($staticPageIdToUrl[$staticPageId])) {
                unset($staticPages[$staticPageId]);
                continue;
            }
            $catalogUrl = $staticPageIdToUrl[$staticPageId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($staticPages[$staticPageId]);
                continue;
            }
            $staticPages[$staticPageId][StaticPagesRepository::FIELD_URL] = $catalogUrl->getUrl();
        }

        $parents = [];
        $menu = $menuService->getAllMenuItems();
        foreach ($menu as $item) {
            if ($item[Menu::FIELD_LEVEL] === Menu::VALUE_FIRST_LEVEL) {
                $parents[] = $item;
            }
        }

        return $this->render('admin/menu/add.html.twig', [
            'departments' => $departments,
            'categories' => $categories,
            'staticPages' => $staticPages,
            'parents' => $parents
        ]);
    }

    /**
     * @param int $id
     * @param DepartmentService $departmentService
     * @param StaticPageService $staticPageService
     * @param UrlTypeService $urlTypeService
     * @param MenuService $menuService
     * @param UrlsRepository $catalogUrlsRepository
     * @param CategoriesService $categoriesService
     * @return JsonResponse|Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function edit(
        int $id,
        DepartmentService $departmentService,
        StaticPageService $staticPageService,
        UrlTypeService $urlTypeService,
        MenuService $menuService,
        UrlsRepository $catalogUrlsRepository,
        CategoriesService $categoriesService
    ) {
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action! Попробуйте еще раз!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $menu = $this->request->get('menuItem');
                    $menuService->setMenuItem($menu);
                    $message = 'Элемент успешно добавлен в главное меню!';
                    break;
            }

            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        $menuItem = $menuService->getItemById($id);
        if (empty($menuItem)) {
            throw $this->createNotFoundException('Элемень меню не найден!');
        }
        $departments = $departmentService->getAllDepartments();
        $departmentUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_DEPARTMENT);
        $departmentUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $departmentUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($departments)
        ]);
        $departmentIdToUrl = [];
        foreach ($departmentUrls as $departmentUrl) {
            $departmentIdToUrl[$departmentUrl->getEntityId()] = $departmentUrl;
        }
        foreach ($departments as $departmentId => $department) {
            if (!isset($departmentIdToUrl[$departmentId])) {
                unset($departments[$departmentId]);
                continue;
            }
            /** @var Urls $catalogUrl */
            $catalogUrl = $departmentIdToUrl[$departmentId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($departments[$departmentId]);
                continue;
            }
            $departments[$departmentId][DepartmentsRepository::FIELD_URL] = $catalogUrl->getUrl();
        }

        $categories = $categoriesService->getAllCategories();
        $categoryUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_CATEGORY);
        $categoryUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $categoryUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($categories)
        ]);
        $categoryIdToUrl = [];
        foreach ($categoryUrls as $categoryUrl) {
            $categoryIdToUrl[$categoryUrl->getEntityId()] = $categoryUrl;
        }
        foreach ($categories as $categoryId => $category) {
            if (!isset($categoryIdToUrl[$categoryId])) {
                unset($categories[$categoryId]);
                continue;
            }
            $catalogUrl = $categoryIdToUrl[$categoryId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($categories[$categoryId]);
                continue;
            }
            $categories[$categoryId][Urls::FIELD_URL] = $catalogUrl->getUrl();
        }

        $staticPages = $staticPageService->getAllStaticPages();
        $staticPageUrlTypeId = $urlTypeService->getUrlTypeIdByUrlType(UrlType::URL_TYPE_STATIC_PAGE);
        $staticPageUrls = $catalogUrlsRepository->findBy([
            UrlsRepository::PROPERTY_URL_TYPE_ID => $staticPageUrlTypeId,
            UrlsRepository::PROPERTY_ENTITY_ID => array_keys($staticPages)
        ]);
        $staticPageIdToUrl = [];
        foreach ($staticPageUrls as $staticPageUrl) {
            $staticPageIdToUrl[$staticPageUrl->getEntityId()] = $staticPageUrl;
        }
        foreach ($staticPages as $staticPageId => $staticPage) {
            if (!isset($staticPageIdToUrl[$staticPageId])) {
                unset($staticPages[$staticPageId]);
                continue;
            }
            $catalogUrl = $staticPageIdToUrl[$staticPageId];
            if ($catalogUrl->getOpen() === UrlsRepository::VALUE_OPEN_NO) {
                unset($staticPages[$staticPageId]);
                continue;
            }
            $staticPages[$staticPageId][StaticPagesRepository::FIELD_URL] = $catalogUrl->getUrl();
        }

        $parents = [];
        $menu = $menuService->getAllMenuItems();
        foreach ($menu as $item) {
            if ($item[Menu::FIELD_LEVEL] === Menu::VALUE_FIRST_LEVEL) {
                $parents[] = $item;
            }
        }

        return $this->render('admin/menu/edit.html.twig', [
            'departments' => $departments,
            'categories' => $categories,
            'staticPages' => $staticPages,
            'menuItem' => $menuItem,
            'parents' => $parents
        ]);
    }

    /**
     * @param $id
     * @param MenuService $menuService
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(
        $id,
        MenuService $menuService
    ) {
        $item = $menuService->getItemById($id);
        if (empty($item)) {
            throw $this->createNotFoundException('Запись с id ' . $id . ' не найдена!');
        }

        $menuService->deleteItemById($id);
        $message = 'Элемент успешно удален из списка меню!';

        return new JsonResponse(['message' => $message]);
    }
}