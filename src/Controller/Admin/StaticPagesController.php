<?php
/**
 * Created by PhpStorm.
 * User: Usser
 * Date: 10.03.2019
 * Time: 12:35
 */

namespace App\Controller\Admin;

use App\Repository\StaticPagesRepository;
use App\Service\StaticPage\StaticPageService;
use App\Service\Url\UrlService;
use Symfony\Component\HttpFoundation\JsonResponse;

class StaticPagesController extends BaseController
{
    public function staticPages(
        StaticPageService $staticPageService
    ) {
        $staticPages = $staticPageService->getAllStaticPages();

        return $this->render('admin/static-pages/static-pages.html.twig', [
            'staticPages' => $staticPages
        ]);
    }

    /**
     * @param StaticPageService $staticPageService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(
        StaticPageService $staticPageService
    ) {
        $action = $this->request->get('action');
        if (!is_null($action)) {
            $message = 'Статическая страница успешно создана!';
            switch ($action) {
                case self::ACTION_SAVE:
                    $staticPage = $this->request->get('staticPage');
                    $staticPage[StaticPagesRepository::FIELD_URL]
                        = UrlService::formatTableUri($staticPage[StaticPagesRepository::FIELD_URL]);
                    $staticPageService->setStaticPage($staticPage);
                    break;
                default:
                    break;
            }
            return new JsonResponse(['message' => $message]);
        } else {
            return $this->render('admin/static-pages/create.html.twig');
        }
    }

    /**
     * @param $id
     * @param StaticPageService $staticPageService
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(
        $id,
        StaticPageService $staticPageService
    ) {
        $staticPage = $staticPageService->getStaticPageEntryById($id);
        if (empty($staticPage)) {
            throw $this->createNotFoundException('Статическая страница не найдена!');
        }
        $staticPageService->deleteStaticPageById($id);
        $message = 'Статическая страница успешно удалена!';

        return new JsonResponse([self::KEY_MESSAGE => $message]);
    }

    /**
     * @param $id
     * @param StaticPageService $staticPageService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(
        $id,
        StaticPageService $staticPageService
    ) {
        $staticPageEntry = $staticPageService->getStaticPageEntryById($id);
        if (empty($staticPageEntry)) {
            throw $this->createNotFoundException('Статическая страница не найдена!');
        }
        $action = $this->request->get('action');
        if (!empty($action)) {
            $message = 'Неверный action. Попробуйте еще раз';
            switch ($action) {
                case self::ACTION_SAVE:
                    $staticPage = $this->request->get('staticPage');
                    $staticPage[StaticPagesRepository::FIELD_URL]
                        = UrlService::formatTableUri($staticPage[StaticPagesRepository::FIELD_URL]);
                    $staticPageService->setStaticPage($staticPage);
                    $message = 'Изменения успешно сохранены!';
                    break;
            }
            return new JsonResponse([self::KEY_MESSAGE => $message]);
        }

        return $this->render('admin/static-pages/edit.html.twig', [
            'staticPage' => $staticPageEntry
        ]);
    }
}
