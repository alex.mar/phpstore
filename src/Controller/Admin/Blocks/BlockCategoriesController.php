<?php

namespace App\Controller\Admin\Blocks;

use App\Controller\Admin\BaseController;
use App\Entity\BlockElementCategories;
use App\Entity\Categories;
use App\Entity\Departments;
use App\Repository\BlockElementCategoriesRepository;
use App\Service\Department\DepartmentService;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;

class BlockCategoriesController extends BaseController
{
    /**
     * @param DepartmentService $departmentService
     * @param DepartmentCategoriesService $departmentCategoriesService
     * @param BlockElementCategoriesRepository $blockElementCategoriesRepository
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function index(
        DepartmentService $departmentService,
        DepartmentCategoriesService $departmentCategoriesService,
        BlockElementCategoriesRepository $blockElementCategoriesRepository
    ): Response {
        $departments = $departmentService->getAllDepartments();
        foreach ($departments as $index => $department) {
            $departments[$index]['selected'] = 0;
        }
        $content = [];

        $action = $this->request->get('action');
        switch ($action) {
            case 'show':
                $departmentId = (int) $this->request->get('departmentId');
                if (empty($departmentId)) {
                    break;
                }
                foreach ($departments as $index => $department) {
                    if ((int) $department[Departments::FIELD_ID] === $departmentId) {
                        $departments[$index]['selected'] = 1;
                    }
                }
                $departmentRelations = $departmentCategoriesService->getDepartmentRelationsByDepartmentId(
                    $departmentId
                );
                if (!empty($departmentRelations)) {
                    $categories = $departmentRelations[DepartmentCategoriesService::KEY_CATEGORIES];
                    $categoryIds = array_keys($categories);
                    $elements = $blockElementCategoriesRepository->getElementsContent($departmentId, $categoryIds);
                    foreach ($categories as $category) {
                        $categoryId = $category[Categories::FIELD_ID];
                        /** @var BlockElementCategories $element */
                        $element = $elements[$categoryId] ?? [];
                        $content[] = [
                            BlockElementCategories::FIELD_ID => !empty($element) ? $element->getId() : 0,
                            BlockElementCategories::FIELD_CATEGORY_ID => $categoryId,
                            Categories::FIELD_NAME => $category[Categories::FIELD_NAME],
                            BlockElementCategories::FIELD_TITLE => !empty($element) ? $element->getTitle() : '',
                            BlockElementCategories::FIELD_DESCRIPTION
                            => !empty($element) ? $element->getDescription() : '',
                            BlockElementCategories::FIELD_IMG => !empty($element) ? $element->getImg() : '',
                            BlockElementCategories::FIELD_ALT => !empty($element) ? $element->getAlt() : '',
                            BlockElementCategories::FIELD_ORDER => !empty($element) ? $element->getOrder() : ''
                        ];
                    }
                }
                break;
            case 'save':
                $elements = $this->request->get('elements');
                $blockElementCategoriesRepository->setElementsContent($elements);

                return $this->json([self::KEY_MESSAGE => 'Изменения успешно сохранены']);
                break;
            default:
                break;
        }

        return $this->render('admin/blocks/categories/index.html.twig', [
            'departments' => $departments,
            'content' => $content
        ]);
    }
}
