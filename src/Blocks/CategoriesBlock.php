<?php

namespace App\Blocks;

use App\Entity\BlockElementCategories;
use App\Entity\Categories;
use App\Entity\Urls;
use App\Repository\ArticlesRepository;
use App\Repository\BlockElementCategoriesRepository;
use App\Repository\UrlsRepository;
use App\Service\DepartmentCategory\DepartmentCategoriesService;
use App\Service\Url\UrlService;
use App\UrlData\UrlData;

class CategoriesBlock implements BlockInterface
{
    /** @var BlockElementCategoriesRepository */
    private $blockElementCategoriesRepository;

    /** @var DepartmentCategoriesService */
    private $departmentCategoriesService;

    /** @var UrlsRepository */
    private $urlsRepository;

    /** @var ArticlesRepository */
    private $articlesRepository;

    public function __construct(
        DepartmentCategoriesService $departmentCategoriesService,
        BlockElementCategoriesRepository $blockElementCategoriesRepository,
        UrlsRepository $urlsRepository,
        ArticlesRepository $articlesRepository
    ) {
        $this->departmentCategoriesService = $departmentCategoriesService;
        $this->blockElementCategoriesRepository = $blockElementCategoriesRepository;
        $this->urlsRepository = $urlsRepository;
        $this->articlesRepository = $articlesRepository;
    }

    /**
     * @param UrlData $urlData
     * @return array
     */
    public function loadContent(UrlData $urlData): array
    {
        $departmentId = $urlData->getDepartmentId();
        $departmentRelations = $this->departmentCategoriesService->getDepartmentRelationsByDepartmentId($departmentId);
        $categories = $departmentRelations[DepartmentCategoriesService::KEY_CATEGORIES];
        $categoryIds = array_keys($categories);
        $urls = $this->urlsRepository->getCategoryUrls(array_keys($categories), $departmentId);
        $urls = array_column($urls, Urls::FIELD_URL, Urls::FIELD_ENTITY_ID);
        foreach ($categories as $index => $category) {
            if (!isset($urls[$category[Categories::FIELD_ID]])) {
                unset($categories[$index]);
                continue;
            }
            $categories[$index][Urls::FIELD_URL] = $urls[$category[Categories::FIELD_ID]];
        }

        $elements = $this->blockElementCategoriesRepository->findBy([
            BlockElementCategories::FIELD_DEPARTMENT_ID => $departmentId,
            BlockElementCategories::FIELD_CATEGORY_ID => $categoryIds
        ]);
        $categoryElements = [];
        foreach ($elements as $element) {
            $categoryElements[$element->getCategoryId()] = $element;
        }

        $result = [];
        foreach ($categories as $category) {
            $count = $this->articlesRepository->getCountArticlesByCategoryId($category[Categories::FIELD_ID]);
            $title = $category[Categories::FIELD_NAME];
            $description = '';
            $img = 'images/images-soon.png';
            $alt = 'image coming soon';
            if (isset($categoryElements[$category[Categories::FIELD_ID]])) {
                /** @var BlockElementCategories $element */
                $element = $categoryElements[$category[Categories::FIELD_ID]];
                if (!empty($element->getTitle())) {
                    $title = $element->getTitle();
                }
                $description = $element->getDescription();
                $img = $element->getImg();
                $alt = $element->getAlt();
            }
            $result[] = [
                'title' => $title,
                'description' => $description,
                'link' => UrlService::formatUri($category[Urls::FIELD_URL]),
                'img' => $img,
                'alt' => $alt,
                'count' => $count
            ];
        }

        return $result;
    }
}
