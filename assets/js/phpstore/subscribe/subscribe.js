let app = require('../app');

import '../../../css/phpstore/subscribe/styles.css';
import '../../../css/phpstore/subscribe/media.css';
import '../../../css/phpstore/views/form/styles.css';
import '../../../css/phpstore/views/form/media.css';

$(document).ready(function () {
    $('.departments-selector').on('change', function () {
        let departmentId = $(this).val();
        let selectorCategories = $('.categories-selector');
        selectorCategories.find('option').remove().end().append('<option value="0">Все</option>');
        if (departmentId == 0) {
            selectorCategories.attr('disabled', true);
            return false;
        }

        app.loadingOn();
        $.ajax({
            url: '/subscribe/',
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'loadCategories',
                departmentId: departmentId
            },
            success: function (response) {
                let categories = response.categories;
                $.each(categories, function (categoryId, category) {
                    selectorCategories.append('<option value="' + categoryId + '">' + category.name + '</option>')
                });
                $('.categories-selector').prop('disabled', false);
                app.loadingOff();
            },
            error: function () {
                app.loadingOff();
                alert('Что то пошло не так! Попробуйте позже');
            }
        });

        return false;
    });
});