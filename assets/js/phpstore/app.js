const $ = require('jquery');
require('mmenu-js');
require('mmenu-js/dist/mmenu.css');
require('mburger-css');
require('mburger-css/dist/mburger.css');
require('bootstrap');
require('bootstrap/dist/css/bootstrap.css');
require('@fortawesome/fontawesome-free/js/all');
require('@fortawesome/fontawesome-free/css/all.css');
require('magnific-popup');
require('magnific-popup/dist/magnific-popup.css');
require('../../libs/js/modal-loading/modal-loading');
require('../../libs/css/modal-loading/modal-loading.css');
require('../../libs/css/modal-loading/modal-loading-animate.css');

import '../../css/phpstore/styles.css';
import '../../css/phpstore/media.css';
import '../../css/phpstore/views/menu/styles.css';
import '../../css/phpstore/views/menu/media.css';

let loading;

export function loadingOn()
{
    loading = new Loading({
        loadingPadding: 8,
        loadingBorderRadius: 4,
        animationWidth: 35,
        animationHeight: 35
    });
}

export function loadingOff()
{
    loading.out();
}

export function validateForm(array)
{
    let bool = true;
    if (array.length === 0) {
        bool = false;
    } else {
        $.each(array, function (key, value) {
            if ($.trim(value).length === 0 || $.trim(value.value).length === 0) {
                bool = false;
            }
        });
    }

    return bool;
}

$(document).ready(function () {
    $('.btn-mmenu').on('click', function () {
        $('#main-menu').mmenu({
            "navbar": {
                "add": false
            },
            "extensions": [
                "theme-dark",
                "pagedim-black",
                "border-full"
            ]
        });
    });
    $('.btn-search-popup-link').magnificPopup({
        type:'inline',
        alignTop: true,
        focus: '#focus-search-input'
    });

    $('.btn-products-sorting').magnificPopup({
        type:'inline',
        midClick: true
    });

    $('.btn-search-popup').on('click', function () {
        let query = $('.input-search-popup').val();
        if ($.trim(query).length === 0) {
            return false;
        }
        window.location.href = window.location.origin + '/search/?q=' + query;

        return false;
    });

    $('.input-search-popup').on('keypress', function (e) {
        if (e.keyCode === 13) {
            let query = $('.input-search-popup').val();
            if ($.trim(query).length === 0) {
                return false;
            }
            let url = window.location.origin + '/search/?q=' + query;
            $(location).attr('href', url);

            return false;
        }
    });

    $('#products-sorting-selector-popup ul li').on('click', function () {
        if ($(this).hasClass('active')) {
            return false;
        }
        let sort = $(this).find('span').data('sort');
        let url = document.location.protocol +"//" + window.location.hostname + window.location.pathname + '?sort=' + sort;

        loadingOn();

        window.location.href = url;

        return false;
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').on('click',function () {
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;
    });
});