import '../app';
require('lightslider');
require('lightslider/dist/css/lightslider.css');

import '../../../css/phpstore/index/styles.css';
import '../../../css/phpstore/index/media.css';
import '../../../css/phpstore/views/catalog-article/styles.css';
import '../../../css/phpstore/views/catalog-article/media.css';

$('#home-gallery-slider').lightSlider({
    item: 1,
    loop:true,
    autoWidth: false,
    slideMargin: 60,
    adaptiveHeight:false,
    auto: true,
    speed: 1000,
    pause: 3000
});