let app = require('../app');

import '../../../css/phpstore/department/styles.css';
import '../../../css/phpstore/department/media.css';
import '../../../css/phpstore/views/breadcrumbs/styles.css';
import '../../../css/phpstore/views/breadcrumbs/media.css';
import '../../../css/phpstore/views/h1/styles.css';
import '../../../css/phpstore/views/h1/media.css';
import '../../../css/phpstore/views/block-categories/styles.css';
import '../../../css/phpstore/views/block-categories/media.css';
import '../../../css/phpstore/views/block-products/styles.css';
import '../../../css/phpstore/views/block-products/media.css';
import '../../../css/phpstore/views/catalog-article/styles.css';
import '../../../css/phpstore/views/catalog-article/media.css';
import '../../../css/phpstore/views/pagination/styles.css';
import '../../../css/phpstore/views/pagination/media.css';


$(document).ready(function () {
    let categoryItem = $('.category-item');

    categoryItem.on('mouseover',function () {
        $(this).find('p.title').css('text-decoration', 'underline');
    });
    categoryItem.on('mouseout',function () {
        $(this).find('p.title').css('text-decoration', 'none');
    });

    $('.selector-sorting-articles').on('change', function () {
        app.loadingOn();
        let sort = $(this).val();
        window.location.href = window.location.pathname + '?sort=' + sort;

        return false;
    });
});