$('.btn-registration').on('click', function () {
    let userData = $('.form-registration').serializeArray();
    if (validateForm(userData) === false) {
        $('.notice-empty-fields').css('display', 'block');
        return false;
    } else {
        $('.notice-empty-fields').css('display', '');
    }
});