import '../app';
require('@fancyapps/fancybox');
require('@fancyapps/fancybox/dist/jquery.fancybox.css');

import '../../../css/phpstore/article/styles.css';
import '../../../css/phpstore/article/media.css';
import '../../../css/phpstore/views/breadcrumbs/styles.css';
import '../../../css/phpstore/views/breadcrumbs/media.css';
import '../../../css/phpstore/views/catalog-article/styles.css';
import '../../../css/phpstore/views/catalog-article/media.css';

let blockRating = $('.rating');

function getCountUp(count)
{
    let ratingCount = $('.rating-count');
    if (count === 0) {
        ratingCount.removeClass('count-red').addClass('count-green');
        count = '+' + (count + 1);
    } else if (count === -1) {
        ratingCount.removeClass('count-red');
        count = 0;
    } else if (count > 0) {
        count = '+' + (count + 1);
        ratingCount.removeClass('count-green').addClass('count-green');
    } else if (count < 0) {
        count = count + 1;
        ratingCount.removeClass('count-red').addClass('count-red');
    }

    return count;
}

function getCountDown(count)
{
    let ratingCount = $('.rating-count');
    if (count === 0) {
        ratingCount.removeClass('count-green').addClass('count-red');
        count = -1;
    } else if (count === 1) {
        ratingCount.removeClass('count-green');
        count = 0;
    } else if (count > 0) {
        count = '+' + (count - 1);
        ratingCount.removeClass('count-green').addClass('count-green');
    } else if (count < 0) {
        count = count - 1;
        ratingCount.removeClass('count-red').addClass('count-red');
    }

    return count;
}

blockRating.on('click', '.fa-arrow-alt-circle-up', function () {
    let ratingCountUp = $('.rating-count-up');
    let ratingCountDown = $('.rating-count-down');
    let countUp = parseInt(ratingCountUp.text());
    let countDown = parseInt(ratingCountDown.text());
    let vote = 0;
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        countUp = countUp - 1;
    } else {
        let iconDown = $('.fa-arrow-alt-circle-down');
        if (iconDown.hasClass('active')) {
            iconDown.removeClass('active');
            countDown = countDown + 1;
            ratingCountDown.text(countDown);
        }
        $(this).addClass('active');
        vote = 1;
        countUp = countUp + 1;
    }
    ratingCountUp.text('+' + countUp);
    if (countUp === 0) {
        ratingCountUp.text(0);
    }

    let articleId = $('.article').data('article-id');
    $.ajax({
        url: '/prod/article/update/rating/',
        data: {
            articleId: articleId,
            ratingUp: countUp,
            ratingDown: countDown,
            vote: vote
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
        },
        error: function () {
        }
    });

    return false;
});

blockRating.on('click', '.fa-arrow-alt-circle-down', function () {
    let ratingCountUp = $('.rating-count-up');
    let ratingCountDown = $('.rating-count-down');
    let countUp = parseInt(ratingCountUp.text());
    let countDown = parseInt(ratingCountDown.text());
    let vote = 0;
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        countDown = countDown + 1;
    } else {
        let iconUp = $('.fa-arrow-alt-circle-up');
        if (iconUp.hasClass('active')) {
            iconUp.removeClass('active');
            countUp = countUp - 1;
            ratingCountUp.text('+' + countUp);
            if (countUp === 0) {
                ratingCountUp.text(0);
            }
        }
        $(this).addClass('active');
        countDown = countDown - 1;
        vote = -1;
    }
    ratingCountDown.text(countDown);

    let articleId = $('.article').data('article-id');
    $.ajax({
        url: '/prod/article/update/rating/',
        data: {
            articleId: articleId,
            ratingUp: countUp,
            ratingDown: countDown,
            vote: vote
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
        },
        error: function () {
        }
    });

    return false;
});