import '../app';

import '../../../css/phpstore/views/catalog-article/styles.css';
import '../../../css/phpstore/views/catalog-article/media.css';
import '../../../css/phpstore/views/breadcrumbs/styles.css';
import '../../../css/phpstore/views/breadcrumbs/media.css';
import '../../../css/phpstore/search-page/styles.css';
import '../../../css/phpstore/search-page/media.css';