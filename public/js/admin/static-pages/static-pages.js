$('.btn-create-static-page').on('click', function () {
    let generalData = $('.form-general').serializeArray();
    let metaData = $('.form-meta-data').serializeArray();
    let staticPageData = $.merge(generalData, metaData);

    if (validateForm(staticPageData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    staticPageData = formatKeyValueArray(staticPageData);

    loadingOn();
    $.ajax({
        url: '/admin/static-page/create/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            staticPage: staticPageData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-delete-static-page').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить статическую страницу?')) {
        return false;
    }

    loadingOn();
    let staticPageId = $(this).data('static-page-id');
    $.ajax({
        url: '/admin/static-page/delete/' + staticPageId + '/',
        dataType: 'json',
        type: 'POST',
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    });

    return false;
});

$('.btn-edit-static-page').on('click', function () {
    let generalData = $('.form-general').serializeArray();
    let metaData = $('.form-meta-data').serializeArray();
    let staticPageData = $.merge(generalData, metaData);

    if (validateForm(staticPageData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    staticPageData = formatKeyValueArray(staticPageData);

    let staticPageId = $(this).data('static-page-id');
    staticPageData['id'] = staticPageId;

    loadingOn();
    $.ajax({
        url: '/admin/static-page/edit/' + staticPageId + '/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            staticPage: staticPageData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});