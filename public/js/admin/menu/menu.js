$('.btn-save-order').on('click', function () {
    let inputs = $('.table-menu').find("input.item-order-test");
    let menuItemOrder = [];
    $.each(inputs, function (key, value) {
        menuItemOrder.push({
            'id': $(this).data('menu-item-id'),
            'order': value.value
        });
    });

    loadingOn();
    $.ajax({
        url: '/admin/menu/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            menuOrder: menuItemOrder
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    });

    return false;
});

$('.btn-delete-menu-item').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить элемент меню?')) {
        return false;
    }

    loadingOn();
    let itemId = $(this).data('menu-item-id');
    $.ajax({
        url: '/admin/menu/delete/' + itemId + '/',
        dataType: 'json',
        type: 'POST',
        data: {
            id: itemId
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже!');
        }
    });

    return false;
});

$('.btn-add-menu-item').on('click', function () {
    let itemData = $('.form-general').serializeArray();

    if (validateForm(itemData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    itemData = formatKeyValueArray(itemData);

    loadingOn();
    $.ajax({
        url: '/admin/menu/add/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            menuItem: itemData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-edit-menu-item').on('click', function () {
    let itemData = $('.form-general').serializeArray();
    if (validateForm(itemData) === false) {
        alert('Все поля должны быть заполнены!');
        return false;
    }
    itemData = formatKeyValueArray(itemData);

    let menuItemId = $(this).data('menu-item-id');
    itemData['id'] = menuItemId;

    loadingOn();
    $.ajax({
        url: '/admin/menu/edit/' + menuItemId + '/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            menuItem: itemData
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-add-url-department').on('click', function () {
    let departmentUrl = $('.selector-departments option:selected').val();
    $('.menu-item-url').val(departmentUrl);

    return false;
});

$('.btn-add-url-category').on('click', function () {
    let categoryUrl = $('.selector-categories option:selected').val();
    $('.menu-item-url').val(categoryUrl);

    return false;
});

$('.btn-add-url-static-page').on('click', function () {
    let staticPageUrl = $('.selector-static-pages option:selected').val();
    $('.menu-item-url').val(staticPageUrl);

    return false;
});

$('.selector-level').on('change', function () {
    let level = parseInt($(this).val());
    if (level === 1) {
        $('.selector-parents').attr('disabled', true);
    } else {
        $('.selector-parents').attr('disabled', false);
    }
});