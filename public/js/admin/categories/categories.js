$('.btn-delete-category').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить категорию?')) {
        return false;
    }

    loadingOn();
    let categoryId = $(this).data('category-id');
    $.ajax({
        url: '/admin/category/delete/' + categoryId + '/',
        data: {
            id: categoryId
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });

    return false;
});

$('.btn-save-category').on('click', function () {
    let category = getCategoryData();
    if (category === false) {
        return false;
    }

    loadingOn();
    $.ajax({
        url: '/admin/category/create/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            category: category
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });
    return false;
});

$('.btn-edit-category').on('click', function () {
    let category = getCategoryData();
    if (category === false) {
        return false;
    }
    let categoryId = $(this).data('category-id');
    category['id'] = categoryId;
    loadingOn();
    $.ajax({
        url: '/admin/category/edit/' + categoryId + '/?action=save',
        dataType: 'json',
        type: 'POST',
        data: {
            category: category
        },
        success: function (response) {
            loadingOff();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert('Что то пошло не так! Попробуйте позже');
        }
    });
    return false;
});

$('.btn-add-department').on('click', function () {
    let tableCategoryDepartments = $('.table-category-departments');

    let categoryDepartmentIds = [];
    tableCategoryDepartments.find('tr.row-category-department').each(function () {
        categoryDepartmentIds.push(parseInt($(this).data('department-item-id')));
    });

    let departmentId =parseInt($('.department-list option:selected').val());
    if ($.inArray(departmentId, categoryDepartmentIds) !== -1) {
        alert('Категори уже привязана к департменту!');
        return false;
    }
    tableCategoryDepartments.append(getCategoryDepartmentItem());

    return false;
});

$('.table-category-departments').on('click', '.btn-delete-category-department', function () {
    let departmentName = $(this).data('department');
    if (!confirm('Вы уверены что хотите удалить привязку категории с департментом ' + departmentName + '?')) {
        return false;
    }
    $(this).closest('tr').remove();
    return false;
});

$('.btn-delete-category-department-relation').on('click', function () {
    let departmentName = $(this).data('department');
    if (!confirm('Вы уверены что хотите удалить привязку категории с департментом ' + departmentName + '?')) {
        return false;
    }

    let relationId = $(this).data('relation-id');
    let categoryId = $(this).data('category-id');
    loadingOn();
    $.ajax({
        url: '/admin/category/edit/' + categoryId + '/?action=deleteRelation',
        context: this,
        dataType: 'json',
        type: 'POST',
        data: {
            relationId: relationId
        },
        success: function (response) {
            loadingOff();
            $(this).closest('tr').remove();
            alert(response.message);
        },
        error: function () {
            loadingOff();
            alert(ERROR_MESSAGE_AJAX_BROKEN);
        }
    });

    return false;
});

function getCategoryDepartmentItem()
{
    let selectedDepartment = $('.department-list option:selected');
    let departmentId = selectedDepartment.val();
    let departmentName = selectedDepartment.text();
    return '<tr class="row-category-department" data-department-item-id="' + departmentId + '">\n' +
        '                        <td>\n' +
        '                            <p class="category-department">' + departmentName + '</p>\n' +
        '                        </td>\n' +
        '                        <td>\n' +
        '<form>' +
        '                            <table class="table table-striped table-hover">\n' +
        '                                <tr>\n' +
        '                                    <td>\n' +
        '                                        <label>Title:</label>\n' +
        '                                    </td>\n' +
        '                                    <td>\n' +
        '                                        <input type="text" name="title"/>\n' +
        '                                    </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                    <td>\n' +
        '                                        <label>H1:</label>\n' +
        '                                    </td>\n' +
        '                                    <td>\n' +
        '                                        <input type="text" name="h1"/>\n' +
        '                                    </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                    <td>\n' +
        '                                        <label>H1 span:</label>\n' +
        '                                    </td>\n' +
        '                                    <td>\n' +
        '                                        <input type="text" name="h1Span"/>\n' +
        '                                    </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                    <td>\n' +
        '                                        <label>Keywords:</label>\n' +
        '                                    </td>\n' +
        '                                    <td>\n' +
        '                                        <input type="text" name="keywords"/>\n' +
        '                                    </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                    <td>\n' +
        '                                        <label>Description:</label>\n' +
        '                                    </td>\n' +
        '                                    <td>\n' +
        '                                        <input type="text" name="description"/>\n' +
        '                                    </td>\n' +
        '                                </tr>\n' +
        '                            </table>\n' +
        '</form>' +
        '                        </td>\n' +
        '                        <td style="text-align: center; width: 100px;">\n' +
        '                            <a href="" class="btn-delete-category-department" data-department="' + departmentName + '" data-department-id="' + departmentId + '"><span class="i-delete"><i class="fas fa-trash-alt"></i></span></a>\n' +
        '                        </td>\n' +
        '                    </tr>';
}

function getCategoryData()
{
    let isValid = true;
    let generalData = $('.form-general').serializeArray();
    if (validateForm(generalData) === false) {
        alert(ERROR_MESSAGE_EMPTY_FIELDS);
        isValid = false;
    }
    generalData = formatKeyValueArray(generalData);
    let tableMetaData = $('.table-category-departments');
    let rowsDepartment = tableMetaData.find('tr.row-category-department');
    if (rowsDepartment.length === 0) {
        alert('Привяжите категорию к департменту!');
        isValid = false;
    }
    let categoryDepartments = {};
    rowsDepartment.each(function () {
        let departmentId = parseInt($(this).data('department-item-id'));
        let categoryMetaData = $(this).find('form').serializeArray();
        if (validateForm(categoryMetaData) === false) {
            alert(ERROR_MESSAGE_EMPTY_FIELDS);
            isValid = false;
        }
        categoryMetaData = formatKeyValueArray(categoryMetaData);
        categoryDepartments[departmentId] = categoryMetaData;
    });

    let category = generalData;
    category['departments'] = categoryDepartments;

    return isValid ? category : isValid;
}
