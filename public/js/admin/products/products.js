$('.selector-departments').on('change', function () {
    let selectedOption = $('.selector-departments option:selected');
    let departmentId = selectedOption.val();
    $('.btn-create-product').attr('href', '/admin/products/create/?departmentId=' + departmentId);
    $('.btn-show-products').attr('href', '/admin/products/?departmentId=' + departmentId);

    loadingOn();
    $.ajax({
        url: '/admin/products/',
        data: {
            action: 'loadCategories',
            departmentId: departmentId
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            loadingOff();
            let selectorCategories = $('.selector-categories');
            clearSelector(selectorCategories);
            selectorCategories.append('<option value="0">Все категории</option>');
            $.each(response.categories, function (categoryId, category) {
                selectorCategories.append('<option value="' + category.id + '">' + category.name + '</option>');
            });
        },
        error: function () {
            alert(ERROR_MESSAGE_AJAX_BROKEN);
        }
    });

    return false;
});

$('.selector-categories').on('change', function () {
    let selectedDepartmentId = $('.selector-departments option:selected').val();
    let selectedCategoryOption = $('.selector-categories option:selected');
    let categoryId = selectedCategoryOption.val();
    if (categoryId === 0) {
        $('.btn-create-product').attr('href', '/admin/products/create/?departmentId=' + selectedDepartmentId);
        $('.btn-show-products').attr('href', '/admin/products/?departmentId=' + selectedDepartmentId);
    } else {
        $('.btn-create-product').attr('href', '/admin/products/create/?departmentId=' + selectedDepartmentId + '&categoryId=' + categoryId);
        $('.btn-show-products').attr('href', '/admin/products/?departmentId=' + selectedDepartmentId + '&categoryId=' + categoryId);
    }
});

$('.btn-delete-product').on('click', function () {
    if (!confirm('Вы уверены что хотите удалить продукт?')) {
        return false;
    }
    let departmentId = $(this).data('department-id');
    let productId = $(this).data('product-id');

    loadingOn();
    $.ajax({
        url: '/admin/products/delete/',
        data: {
            departmentId: departmentId,
            productId: productId
        },
        type: 'POST',
        dataType: 'json',
        success: function (response) {
            loadingOff();
            alert(response.message);
            location.reload();
        },
        error: function () {
            loadingOff();
            alert(ERROR_MESSAGE_AJAX_BROKEN);
        }
    });

    return false;
});