const ERROR_MESSAGE_EMPTY_FIELDS = 'Все поля должны быть заполнены!';
const ERROR_MESSAGE_AJAX_BROKEN = 'Что то пошло не так! Попробуйте позже';

function loadingOn()
{
     loading = new Loading({

        // // 'ver' or 'hor'
        // direction: 'ver',
        //
        // // loading title
        // title: undefined,
        //
        // // text color
        // titleColor: '#FFF',
        //
        // // font size
        // titleFontSize: 14,
        //
        // // extra class(es)
        // titleClassName: undefined,
        //
        // // font family
        // titleFontFamily:   undefined,
        //
        // // loading description
        // discription: undefined,
        //
        // // text color
        // discriptionColor:  '#FFF',
        //
        // // font size
        // discriptionFontSize: 14,
        //
        // // extra class(es)
        // discriptionClassName: undefined,
        //
        // // font family
        // directionFontFamily: undefined,
        //
        // // width/height of loading indicator
        // loadingWidth: 'auto',
        // loadingHeight: 'auto',

        // padding in pixels
        loadingPadding: 8,

        // // background color
        // loadingBgColor: '#252525',
        //
        // // border radius in pixels
        loadingBorderRadius: 4,
        //
        // // loading position
        // loadingPosition: 'fixed',
        //
        // // shows/hides background overlay
        // mask: true,
        //
        // // background color
        // maskBgColor: 'rgba(0, 0, 0, .6)',
        //
        // // extra class(es)
        // maskClassName: undefined,
        //
        // // mask position
        // maskPosition: 'fixed',
        //
        // // 'images': use a custom images
        // // loading<a href="https://www.jqueryscript.net/animation/">Animation</a>: 'origin',
        //
        // // path to loading spinner
        // animationSrc: undefined,
        //
        //     // width/height of loading spinner
        animationWidth: 35,
        animationHeight: 35,
        // animationOriginWidth: 4,
        // animationOriginHeight: 4,
        //
        // // color
        // animationOriginColor: '#FFF',
        //
        // // extra class(es)
        // animationClassName: undefined,
        //
        // // auto display
        // defaultApply: true,
        //
        // // animation options
        // animationIn: 'animated fadeIn',
        // animationOut: 'animated fadeOut',
        // animationDuration:  1000,
        //
        // // z-index property
        // zIndex: 0
    });
}

function loadingOff()
{
    loading.out();
}

function validateForm(array)
{
    let bool = true;
    if (array.length === 0) {
        bool = false;
    } else {
        $.each(array, function (key, value) {
            if ($.trim(value).length === 0 || $.trim(value.value).length === 0) {
                bool = false;
            }
        });
    }

    return bool;
}

function formatKeyValueArray(array)
{
    let result = {};

    $.each(array, function (key, value) {
        if (value.length === 0 || value.value.length === 0) {
            return true;
        }
        result[value.name] = value.value;
    });

    return result;
}

$('.nav-menu ul').each(function () {
    $(this).prev().addClass('collapsible').click(function () {
        if ($(this).next().css('display') === 'none') {
            $(this).next().slideDown(200, function () {
                $(this).prev().removeClass('collapsed').addClass('expanded');
            });
        } else {
            $(this).next().slideUp(200, function () {
                $(this).prev().removeClass('expanded').addClass('collapsed');
                $(this).find('ul').each(function () {
                    $(this).hide().prev().removeClass('expanded').addClass('collapsed');
                });
            });
        }
        return false;
    });
});

$('.item-sub').on('click', function () {
    $(this).find('[class*="angle"]').toggleClass('fa-angle-up fa-angle-down');
});

function clearSelector(selector)
{
    selector.find('option').remove();
}